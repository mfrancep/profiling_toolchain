import sys

NB_PROC = 16
# /!\ vci_xicu_handle_irq function have to be trigged by the HW
# hence fct_uname1 have to be set to the vci_xicu_handle_irq
VCI_XICU_HANDLE_IRQ_ADDR = 0xc027b978

        
# parse a line of the dump file
# return the time, proc num and pc of the line instruction in an interger format 
def parse_dump_line(line):
    if (line.find("#") >= 0) : # the line is a comment => this the line
        print "comment"
        return (-1, -1, -1, -1, -1);

    #parse le line
    fields = line.split(";");
    if (len(fields) < 8): #the line is invalide
        return (-1, -1, -1, -1, -1);
    
    time = fields[0];
    proc = fields[1];
    pc = fields[2];
    process_id = fields[4]
    instr = fields[7];
    
    return (int(time), int(proc), int(pc), int(process_id), int(instr));

# to identify and to list all process running on a core
def list_processes():
    global processes_list

    for i in range(0,NB_PROC):
        processes_list.append([])
    
    dump_file.seek(0) #place on the top of the file
    while (1):
        dump_line = dump_file.readline()
        if (len(dump_line) == 0):
            break
        curr_time, curr_proc, curr_pc, process_id, instr = parse_dump_line(dump_line)

        if (process_id < 0):
            continue

        if (process_id not in processes_list[curr_proc]):
            processes_list[curr_proc].append(process_id)

    return
        
# parse a line of the disassembled file
# jr  return 0
# jal or jalr return 1
# else return -1
#return instr addr
def parse_assembly_line(line):
    fields = line.split("\t");
    
    if (len(fields) < 3) or (fields[0].find(":") == -1): #not and instruction line
        #print line, "is not an instruction"
        return (-1, -1)
    
    instr_name = fields[2];
    addr = fields[0][:fields[0].find(":")]
    if (instr_name.find("jr") >= 0):
        return (0, int(addr,16))
    elif (instr_name.find("jal") >= 0) :# detecte jal or jalr
        return (1, int(addr,16))
    return (-1, -1)
          
# get the assembly instruction type (jal,jalr or jr) and jump address of the instruction which pc is targeted_pc
# jr  return 0
# jal or jalr return 1
# else return -1
#return instruction addr
def get_assembly_line(targeted_pc):
    line = "line"; #padding
    line_pc = 0;
    dissassembly_file.seek(0) #place on the top of the file
    while ((line_pc != targeted_pc) and (len(line) > 0)):
        line = dissassembly_file.readline()
        #parse
        instr_type, addr = parse_assembly_line(line)
        if ((instr_type >= 0) and (addr == targeted_pc)):
            return (instr_type, addr)
          
    return (-1,-1)

# get the instruction type 
# jr  return 0
# jal or jalr return 1
# else return -1
def get_instruction_type(instr_code):
    if (instr_code&0xFC000000) == 0x0C000000 or (instr_code&0xFC1F07FF) == 0x00000009: #jal or jalr
        return 1
    #elif instr_code == 0x03E00008:
    #    return 0
    elif instr_code == 0x03E00008: #jr ra
        return 0 # return code
    elif instr_code == 0x03200008: #jr t9
        # instruction jr on other register that 'ra' do not correspond to a single return
        # because the execution flow do not return in the calling fonction but jump in other function
        return 2 # return and jump code
    #elif (instr_code&0xFC1FFFFF) == 0x00000008:  # jr and not "jr v0" and not "jr v1" 
     #   if instr_code == 0x03E00008: #jr ra
     #       return 0 # return code
     #   else:  # "jr t9"
     #       # instruction jr on other register that 'ra' do not correspond to a sigle return
     #       # because the execution flow do not return in the calling fonction but jump in other function
     #       return 2 # return and jump code
    return -1

# build a leaf of the call tree 
def leaf(time, pc, core, process_id):
    global nb_indentations
    global call_list
    global call_dict
    global call_dict_short
    global IRQ_nb_indentations
    global IRQ_delays
    
    while (1):
        #print "function leaf on : time ", time, ", pc : ", pc, ", core : ", core
        curr_proc = -1
        curr_process = -1
        curr_pc = -1
        while ((curr_proc != core) or (curr_process != process_id)):
            dump_line = dump_file.readline()

            if (len(dump_line) == 0):
                str_res = "call:" + str(time) + " until: ..."  "=>"
                str_res += str(" unclosed call (time ") + str(time) + ") "
                str_res += str(pc)
                str_res+=str("\n")
                if time not in call_dict:
                    call_dict[time] = str_res
                    call_dict_short[time] = str(nb_indentations)+";unclosed call;"+str(time)+"\n"
                return -1;
            
            curr_time, curr_proc, curr_pc, curr_process, instr = parse_dump_line(dump_line);

        # get assembly line
        #instr_type, addr = get_assembly_line(curr_pc)
        
        # expectional case : interruptions
        # when an interruption occurs execution flow is derouted to IRQ handler
        # we can only detect the event by triggering on the handler function PC
        # since no jump is used to go to this handle
        # /!\ vci_xicu_handle_irq function have to be trigged by the HW
        # hence fct_uname1 have to be set to the vci_xicu_handle_irq
        if curr_pc == VCI_XICU_HANDLE_IRQ_ADDR:
            nb_indentations = nb_indentations + 1
            print dump_line[:-1], " => interrupt (identation  ", str(nb_indentations) , ")" 
            leaf(curr_time, curr_pc, curr_proc, curr_process)
            continue
        
        instr_type = get_instruction_type(instr)
        if (instr_type == 0) or (instr_type == 2): # if jr instruction => close the leaf
            
            # remove interruption processing time if interrupt occured during this function
            if (pc== VCI_XICU_HANDLE_IRQ_ADDR):
                
                if (nb_indentations < IRQ_nb_indentations) : # case of IRQ in IRQ management
                    for i in range(0,nb_indentations):
                         IRQ_delays[i] -= IRQ_delays[nb_indentations]

                IRQ_nb_indentations = nb_indentations
                for i in range(0,IRQ_nb_indentations):
                    if (i>=len(IRQ_delays)):
                        IRQ_delays.append(0)
                    IRQ_delays[i] += (curr_time-time)
            
            str_res = "call:" + str(time) + " ret:" + str(curr_time) + "=>"
            for i in range(0,nb_indentations):
                str_res += "\t"
            if (curr_pc in functions_table):
                str_res += functions_table[curr_pc]
            else:
                str_res += str(curr_pc)
            str_res+=str(" = ")
            str_res+=str(curr_time-time)

            # remove interruption processing time if interrupt occured during this function
            if (nb_indentations < IRQ_nb_indentations) :
                print "!!!! error : nb_indentations = ", nb_indentations, " IRQ delays size  = ", len(IRQ_delays)   
                str_res+=str(" (without IRQ : ")
                if ((nb_indentations > 0) and (nb_indentations < len(IRQ_delays))): 
                    str_res+=str(curr_time-time-IRQ_delays[nb_indentations])
                str_res+=str(")")
                str_res+=str("(")
                str_res+=str(curr_time)
                str_res+=str("-")
                str_res+=str(time)
                str_res+=str("-")
                str_res+=str(IRQ_delays[nb_indentations])
                str_res+=str(" nb indentations = ")
                str_res+=str(nb_indentations)
                str_res+=str(" IRQ indentations = ")
                str_res+=str(IRQ_nb_indentations)
                str_res+=str(")")
                for i in range(nb_indentations,len(IRQ_delays)):
                    IRQ_delays[i] = 0
                IRQ_nb_indentations = nb_indentations # go back up the trig level since only lower indentation will be affect by the IRQ
                
            str_res+=str("\n")
            #call_list.append(str_res):
            call_dict[time] = str_res
            if (curr_pc in functions_table):
                call_dict_short[time] = str(nb_indentations)+";"+functions_table[curr_pc]+";"+str(curr_time-time)+"\n"
            else:
                call_dict_short[time] = str(nb_indentations)+";"+str(curr_pc)+";"+str(curr_time-time)+"\n"

            #case of "jr v0", "jr v1", "jr t9"
            # build tree
            if instr_type == 2:
                print dump_line[:-1], " => return_call " , str_res[:-1], " (indentation  ", str(nb_indentations) , ")" 
                leaf(curr_time, curr_pc, curr_proc, curr_process)
            else:
                # regular case of "jr ra" instruction
                print dump_line[:-1], " => return " , str_res[:-1], " (indentation  ", str(nb_indentations) , ")" 
                nb_indentations = nb_indentations - 1
                return 1
        elif (instr_type == 1): #if jal or jalr instruction => build the tree
            nb_indentations = nb_indentations + 1
            print dump_line[:-1], " => call (indentation  ", str(nb_indentations) , ")" 
            leaf(curr_time, curr_pc, curr_proc, curr_process)
    return 1;


def extact_function_name(assembly_file, user=0):
    line = "line"; #padding
    assembly_file.seek(0) #place on the top of the file
    curr_fct_name = ""
    while (len(line) > 0):
        line = assembly_file.readline()

        # if line is function line => extraction of the funtion name
        if (line.find(">:") >= 0): # if it is a function title
            fields = line.split(" ")
            if (len(fields) < 2): #error in the line
                continue;
            curr_fct_name = fields[1][:fields[1].find(":")]

        # looking for jr instruction
        instr_type, addr = parse_assembly_line(line)
        if (instr_type == 0): #jr instr
            if (user==1):
                functions_table[addr] = "(user)" + curr_fct_name
            else:
                functions_table[addr] = curr_fct_name


if __name__ == "__main__":
    nb_indentations = 0
    dump_file = open("dump_file.txt", 'r')
    dissassembly_file = open("diss_file.txt", 'r')
    dissassembly_file_user = open("diss_file_user.txt", 'r')
    call_list = []
    call_dict = {} #dict of the timed call stack {time:"function str",...}
    call_dict_short = {} #dict of the timed call stack {time:"function str",...}
    # we could reduce the memory size used by these two dictionnary by storing binary info and not string
    # thanks to namedtuple => if the string are composed when the write occured, one dict is enough
    processes_list = []

    functions_table={} #dict of jr function functions_table={@jr_instruction:"function name", ...}

    # Remove IRQ processing globales variables
    IRQ_nb_indentations = 0 # trigger level : indentation lower than this level will be affect by the special processing
    IRQ_delays = [] # time delays spent in the IRQ routine. Delays are cumulated by indentation

    extact_function_name(dissassembly_file)
    extact_function_name(dissassembly_file_user, 1)

    list_processes()

    for i in range (0,NB_PROC):
        output_file_readable = open("r_calltree_"+str(i)+".txt", 'w')
        output_file = open("calltree_"+str(i)+".txt", 'w')
        for process_id in processes_list[i]:
            dump_file.seek(0) #place on the top of the file
            ret = 1
            while(ret >= 0):
                nb_indentations = 0
                curr_proc = -1
                curr_process = -1
                while ((curr_proc != i) or (curr_process != process_id)):

                       dump_line = dump_file.readline()
                       
                       if (len(dump_line) == 0):
                           ret = -1; #exit loop
                           break;
            
                       curr_time, curr_proc, curr_pc, curr_process, instr = parse_dump_line(dump_line);

                if (ret == -1):
                    break;
        
                instr_type = get_instruction_type(instr)
                if (instr_type == 1): #if jal or jalr instruction => build the tree
                    print dump_line[:-1], " => call (identation  ", str(nb_indentations) , ")" 
                    ret = leaf(curr_time,curr_pc,i,curr_process);


        # write output
        #while(len(call_list)>0):
        #    output_file.write(call_list.pop())
        while(len(call_dict) > 0):
            min_key = 0xFFFFFFFF
            for key in call_dict:
                if (key < min_key):
                    min_key = key
            output_file_readable.write(call_dict[min_key])
            output_file.write(call_dict_short[min_key]) # short dict and orginal dict have the same keys
            del call_dict[min_key]
            del call_dict_short[min_key]
            
        output_file_readable.close()
        output_file.close()
            
#        break
                    
    dump_file.close()
    dissassembly_file.close()
                
    print "fin prog"
    
