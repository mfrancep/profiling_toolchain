import sys
import os
import subprocess
import numpy as np
import matplotlib.pyplot as plt

PRINT_IMAGE = 1

INPUT_OFFSET = 4

acquire_delays = []
go2sleep = []
contented_release = []

if (len(sys.argv) < 3):
        print 'Usage python',  sys.argv[0], ' [path to delays file] [nb_proc]'
        sys.exit()

nb_proc = int(sys.argv[2]);
print "print ", sys.argv[1] , "with ", nb_proc, "proc" 
        
import csv
with open(sys.argv[1], 'rb') as csvfile:
    delaysreader = csv.reader(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_NONNUMERIC)
    rownum = 0
    for row in delaysreader:
        subrow = row;
        if len(subrow) > 0:
            subrow.pop(0);
        if len(subrow) > 0:
            subrow.pop();
        for i in range(0, nb_proc):
                if rownum == INPUT_OFFSET+i:
                        acquire_delays.insert(i,subrow);
                if rownum == INPUT_OFFSET+(2*nb_proc)+2+i:
                        go2sleep.insert(i,subrow);   
                if rownum == INPUT_OFFSET+(3*nb_proc)+3+i:
                        contented_release.insert(i,subrow);
                        
        #print ', '.join(row)
        rownum += 1

# lock acquisition delay
        
print "Out of scale lock acquisition delay"
MAX_RANGE = 2000
index = 0
for delays in acquire_delays:
    print "proc", index, ":"
    total = 0;
    for elem in delays:
        if elem > MAX_RANGE:
            print elem, ";" 
            total += 1
    print "sum out of scale for proc", index,":", total, "\n";
    index += 1;

# plot results
plt.figure(1)
plt.subplot(111)
plt.boxplot(acquire_delays)
plt.ylim(0, MAX_RANGE)
plt.title('Lock acquisition delays by core')
plt.xlabel('core number', fontsize=14, color='black')
plt.ylabel('nb cycles', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=0.5)
plt.savefig('lock_acquisition_delays.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "lock_acquisition_delays.png"]) 


# go to sleep delay
print "Out of scale go to sleep delays"
MAX_RANGE = 1500
index = 0
for delays in go2sleep:
    print "proc", index, ":"
    total = 0;
    for elem in delays:
        if elem > MAX_RANGE:
            print elem, ";" 
            total += 1
    print "sum out of scale for proc", index,":", total, "\n";
    index += 1;

# plot results
plt.figure(2)
plt.subplot(111)
plt.boxplot(go2sleep)
plt.ylim(0, MAX_RANGE)
plt.title('Go to sleep delays by core')
plt.xlabel('core number', fontsize=14, color='black')
plt.ylabel('nb cycles', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=0.5)
plt.savefig('go_to_sleep_delays.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "go_to_sleep_delays.png"]) 

# contented release delays
print "Out of scale contented release delays"
MAX_RANGE = 200000
index = 0
for delays in contented_release:
    print "proc", index, ":"
    total = 0;
    for elem in delays:
        if elem > MAX_RANGE:
            print elem, ";" 
            total += 1
    print "sum out of scale for proc", index,":", total, "\n";
    index += 1;

# plot results
plt.figure(3)
plt.subplot(111)
plt.boxplot(contented_release)
plt.ylim(0, MAX_RANGE)
plt.title('Contented release delays by core')
plt.xlabel('core number', fontsize=14, color='black')
plt.ylabel('nb cycles', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=0.5)
plt.savefig('contented_release_delays.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "contented_release_delays.png"]) 


#read lock failed file
failed_lock = {};
import csv
with open('lock_failed_call.txt', 'rb') as csvfile:
    delaysreader = csv.reader(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_NONNUMERIC)
    rownum = 0
    for row in delaysreader:
        #print row, "size " , len(row);
        if (len(row) >= 2):
            failed_lock[row[0]] = row[1];
        #if rownum >= DYN_OFFSET:
            
        #print ', '.join(row)
        rownum += 1

#dynamic data processing
DYN_OFFSET = 2;
dyn_lock={} #dict of sucessful lock acquisition dyn_data={tid1:[t1, t2, ...], tid2=[t1,t2, ...], ...}
dyn_failed={}  #dict of fail lock acquisition (go to sleep) dyn_failed={tid1:[t1, t2, ...], tid2=[t1,t2, ...], ...}
dyn_proc={} #dict of proc for each lock sucessful acquisition dyn_proc={tid1:[p1, p2, ...], tid2=[p1,p2, ...], ...}
dyn_proc_failed={} #dict of proc for each lock failed acquisition dyn_proc_failed={tid1:[p1, p2, ...], tid2=[p1,p2, ...], ...}
contention={} #dict of contention value for each lock contention={lock1:[value1, value2, ...], lock2=[value1,value2, ...], ...}
contention_time={} #dict of contention time for each lock contention={lock1:[t1, t2, ...], lock2=[t1,t2, ...], ...}
fairness=[]
for i in range(0, nb_proc):
        fairness.insert(i,[])
import csv
with open('dynamic_trace.txt', 'rb') as csvfile:
    delaysreader = csv.reader(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_NONNUMERIC)
    rownum = 0
    tempo_time = 0
    tempo_proc = 0
    last_rownum = 0
    for row in delaysreader:
        #print row
        if rownum >= DYN_OFFSET:
            if len(row) < 10:
                print "incomplete data row, size", len(row)
                print row
                continue
            if row[1] == 0: #only "enter in lock function"
                #print row[3]
                tempo_time = row[0]
                tempo_proc = row[2]
                last_rownum = rownum
                #if (row[0] in failed_lock): # lock in failed list
                    #if (failed_lock[row[0]] == row[3]): # check tid
                        # time
                        #if (row[3] not in dyn_failed):
                        #    dyn_failed[row[3]] = [row[0]]
                        #else:
                        #    dyn_failed[row[3]].append(row[0])
                        #proc
                        #if (row[3] not in dyn_proc_failed):
                        #    dyn_proc_failed[row[3]] = [row[2]]
                        #else:
                        #    dyn_proc_failed[row[3]].append(row[2])
                #else:
                    #time
                    #if (row[3] not in dyn_lock):
                    #    dyn_lock[row[3]] = [row[0]]
                    #else:
                    #    dyn_lock[row[3]].append(row[0])
                    #proc
                    #if (row[3] not in dyn_proc):
                    #    dyn_proc[row[3]] = [row[2]]
                    #else:
                    #    dyn_proc[row[3]].append(row[2])
            elif row[1] == 1:
                if last_rownum == (rownum-1):
                    if (tempo_time in failed_lock): # lock in failed list
                        if (failed_lock[tempo_time] == row[3]): # check tid
                            # time
                            if (row[4] not in dyn_failed):
                                dyn_failed[row[4]] = {}
                            else:
                                if (row[3] not in dyn_failed[row[4]]):
                                    dyn_failed[row[4]][row[3]] =[tempo_time]
                                else:
                                    dyn_failed[row[4]][row[3]].append(tempo_time)
                            #proc
                            if (row[4] not in dyn_proc_failed):
                                dyn_proc_failed[row[4]] = {}
                            else:
                                if (row[3] not in dyn_proc_failed[row[4]]):
                                    dyn_proc_failed[row[4]][row[3]] = [tempo_proc]
                                else:
                                    dyn_proc_failed[row[4]][row[3]].append(tempo_proc)
                    else:
                        #time
                        if (row[4] not in dyn_lock):
                            dyn_lock[row[4]] = {}
                        else:
                            if (row[3] not in dyn_lock[row[4]]):
                                dyn_lock[row[4]][row[3]] = [tempo_time]
                            else:
                                dyn_lock[row[4]][row[3]].append(tempo_time)
                        #proc
                        if (row[4] not in dyn_proc):
                            dyn_proc[row[4]] = {}
                        else:
                            if (row[3] not in dyn_proc[row[4]]):
                                dyn_proc[row[4]][row[3]] = [tempo_proc]
                            else:
                                dyn_proc[row[4]][row[3]].append(tempo_proc)
                #else:
                #    print "bad rownum!!! ", row
                
                #contention
                if (row[4] not in contention):
                    contention[row[4]] = [row[5]]
                else:
                    contention[row[4]].append(row[5])
                if (row[4] not in contention_time):
                    contention_time[row[4]] = [row[0]]
                else:
                    contention_time[row[4]].append(row[0])
            elif row[1] == 3: #return from lock
                #fairness
                if (row[10] == ''):
                        fairness[int(row[2])].append(0);
                else:
                        fairness[int(row[2])].append(int(row[10]));
                
        #print ', '.join(row)
        rownum += 1
        
#print "\n\ndyn failed \n" , dyn_failed
#print "\n\ndyn lock \n", dyn_lock

fig_num=4

visu_map = {}
for key, value in dyn_lock.iteritems():
    plt.figure(fig_num)
    fig_num += 1
    my_dpi=128
    plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    plt.subplot(111)
    num_thread = 0
    visu = 'k^'
    if (key in dyn_failed):
        for subkey, subvalue in dyn_failed[key].iteritems():
            if (num_thread == 0):
                visu_map[subkey] = "b";
                visu = 'bx'
            elif (num_thread == 1):
                visu_map[subkey] = "g";
                visu = 'gx'
            elif (num_thread == 2):
                visu_map[subkey] = "r";
                visu = 'rx'
            elif (num_thread == 3):
                visu_map[subkey] = "y";
                visu = 'yx'
            else:
                visu_map[subkey] = "k";
                visu = 'kx'
            plt.plot(subvalue, dyn_proc_failed[key][subkey], visu,  label=subkey);
            num_thread += 1
            
    num_thread = 0
    visu = 'kv'
    for subkey, subvalue in value.iteritems():
        if (num_thread == 0):
            if (subkey not in visu_map):
                visu = 'c+'
            else:
                visu = visu_map[subkey] + "+"
        elif (num_thread == 1):
            if (subkey not in visu_map):
                visu = 'm+'
            else:
                visu = visu_map[subkey] + "+"
        elif (num_thread == 2):
            if (subkey not in visu_map):
                visu = 'b+'
            else:
                visu = visu_map[subkey] + "+"
        elif (num_thread == 3):
            if (subkey not in visu_map):
                visu = 'g+'
            else:
                visu = visu_map[subkey] + "+"
        else:
            visu = 'k+'
        print "tid",key
        plt.plot(subvalue, dyn_proc[key][subkey], visu, label=subkey);
        num_thread += 1
    plt.ylim(-1, (nb_proc+0.5))
    graph_title = "Dynamic_lock_id_"+str(int(key))
    print graph_title
    plt.title(graph_title)
    plt.legend(prop={'size': 7}, ncol=2)
    plt.xlabel('cycles', fontsize=14, color='black')
    plt.ylabel('cores', fontsize=14, color='black')

    plt.subplots_adjust(bottom = 0.2, hspace=0.5)
    graph_title_png = graph_title+".png"
    print graph_title_png
    plt.savefig(graph_title_png, dpi=my_dpi*1.4)
    if PRINT_IMAGE==0:
            subprocess.Popen(["display", graph_title_png]) 


plt.figure(fig_num)
plt.clf()
plt.subplot(111)
num_thread = 0
visu = 'kx'
for key, value in contention.iteritems():
    if (num_thread == 0):
        visu = 'bx'
    elif (num_thread == 1):
        visu = 'gx'
    elif (num_thread == 2):
        visu = 'rx'
    elif (num_thread == 3):
        visu = 'yx'
    else:
        visu = 'kx'
    plt.plot(contention_time[key], value, visu,  label=key);
    num_thread += 1

plt.title('Lock contention')
#plt.legend(prop={'size': 7}, ncol=2)
plt.xlabel('cycles', fontsize=14, color='black')
plt.ylabel('nb of threads wainting for the lock', fontsize=14, color='black')


plt.subplots_adjust(bottom = 0.2, hspace=0.5)
plt.savefig('lock_contention.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "lock_contention.png"])
fig_num += 1

# plot results
print fairness
plt.figure(fig_num)
plt.subplot(111)
plt.boxplot(fairness)
plt.title('Lock unfairness by core')
plt.xlabel('cores', fontsize=14, color='black')
plt.ylabel('nb of lock acquisitions by others \n during the thread waiting phase', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=0.5)
plt.savefig('lock_fairness.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "lock_fairness.png"]) 

fig_num += 1

lock_id= []
lock_ratio=[]
lock_reuse = 0
lock_reuse_cluster = 0
lock_total = 0
reuse_data_flag = 0
import csv
with open("delays.txt", 'rb') as csvfile:
    delaysreader = csv.reader(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_NONNUMERIC)
    rownum = 0
    for row in delaysreader:
        subrow = row
        if len(subrow) > 0:
                if (type(row[0]) is str) and ("reuse" in row[0]):
                        reuse_data_flag = 1;
                        continue;
                if (type(row[0]) is str) and ("lock dynamic" in row[0]):
                        reuse_data_flag = 0;
                        break;
        if reuse_data_flag == 1:
                if subrow[2] > 10 : # only lock which are locked more than ten times are taken into account
                        lock_id.append(subrow[0]);
                        lock_reuse += subrow[1];
                        lock_reuse_cluster += subrow[2];
                        lock_total += subrow[3]; 
                        lock_ratio.append(subrow[4]);

plt.figure(fig_num)
plt.clf()
plt.subplot(111)
first_kernel = 1
first_user = 1
reuse_mean_ratio = 0;
for i in range(0,len(lock_ratio)):
        if (lock_id[i] > 0x4000000) :
                if (first_kernel == 1) :
                        plt.plot(i, lock_ratio[i], 'bx', label="kernel lock");
                        first_kernel = 0;
                else :
                        plt.plot(i, lock_ratio[i], 'bx');
        else :
                if (first_user == 1) :
                        plt.plot(i, lock_ratio[i], 'kx', label="user lock");
                        first_user = 0;
                else :
                        plt.plot(i, lock_ratio[i], 'kx');
                        
# Calculate the simple average of the data
reuse_mean_ratio = (lock_reuse/lock_total)*100;
reuse_mean_ratio_cluster = (lock_reuse_cluster/lock_total)*100;
print "reuse_mean_ratio = ", reuse_mean_ratio, "%"
print "reuse_mean_ratio_cluster = ", reuse_mean_ratio_cluster, "%"

#plt.plot(lock_id, lock_ratio, 's');
plt.title('Lock reuse ratio')
plt.xlabel('lock id', fontsize=14, color='black')
plt.ylabel('reuse ratio in %', fontsize=14, color='black')
plt.legend(prop={'size': 7}, ncol=2)

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('lock_reuse_ratio.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "lock_reuse_ratio.png"])
