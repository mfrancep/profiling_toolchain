import sys
import numpy as np
from collections import namedtuple
import os
import subprocess
import matplotlib
import matplotlib.pyplot as plt

NB_PROCS = 16
PRINT_DEBUG = False
MAX_DELAY = 1500

LockLoc = namedtuple('LockLoc', 'distant local')

# search the line matching with args in the L2 vcii file
# return the time, 0 if no line is matching
def find_L2_vci_line(ref_time, ref_addr_phy_addr, ref_srcid, ref_trdid, ref_pktid):
    global L2_file_offset
    L2_vci_file = open("L2_vci_file.txt", 'r')
    L2_vci_file.seek(L2_file_offset)
    while(1):
        line = L2_vci_file.readline()

        if (len(line) == 0):
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 8):
            print "error : vci line " + line + " not valid"
            continue

        L2_vci_time = long(fields[0],10)
        L2_vci_cluster = int(fields[1],10)
        L2_vci_addr_phy_addr = int(fields[3],10)
        L2_vci_srcid = int(fields[5],10)
        L2_vci_trdid = int(fields[6],10)
        L2_vci_pktid = int(fields[7],10)
        
        if (L2_vci_time < ref_time) :
            L2_file_offset = L2_vci_file.tell()
            continue

        if (L2_vci_time > ref_time+MAX_DELAY):
            return (0, -1)

        if ((L2_vci_addr_phy_addr == ref_addr_phy_addr) and (L2_vci_srcid == ref_srcid) and (L2_vci_trdid == ref_trdid) and (L2_vci_pktid == ref_pktid)):
            return (L2_vci_time, L2_vci_cluster)

    return (0, -1)


if __name__ == "__main__":
    addr_file = open("addr_file.txt", 'r')
    
    outfile = open("L2_delays.txt", 'w')
    outfile.write("#L1_time;L2_in_time;phy_addr;srcid;trdid;pktid;network_delay;init_cluster;mem_cluster\n")

    network_delays = []
    L2_delays = []
    req_distant_cluster = 0
    req_local_cluster = 0
    lock_locality_dic = {}

    L2_file_offset = 0
    
    while(1):
        line = addr_file.readline()
        
        if (len(line) == 0):
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 9):
            print "error : vci line " + line + " not valid"
            continue

        addr_time = long(fields[0],10)
        core_num = long(fields[2],10)
        addr_phy_addr = int(fields[5],10)
        addr_srcid = int(fields[6],10)
        addr_trdid = int(fields[7],10)
        addr_pktid = int(fields[8],10)

        receive_L2_vci_time, cluster_num = find_L2_vci_line(addr_time, addr_phy_addr, addr_srcid, addr_trdid, addr_pktid);
        if (receive_L2_vci_time == 0) :
            print "receive_L2_vci_time does not find for line addr ", line
            continue
        network_delays.append(receive_L2_vci_time-addr_time)

        
        out_str =   str(addr_time) + ";"
        out_str += str(receive_L2_vci_time) + ";"
        out_str += str(addr_phy_addr) + ";"
        out_str += str(addr_srcid) + ";"
        out_str += str(addr_trdid) + ";"
        out_str += str(addr_pktid) + ";"
        out_str += str(receive_L2_vci_time-addr_time) + ";"
        out_str += str(core_num/4)+ ";"
        out_str += str(cluster_num)+ "\n"
        outfile.write(out_str)

        if ((core_num/4) != cluster_num):
            req_distant_cluster += 1
            if addr_phy_addr in lock_locality_dic:
                lock = LockLoc((lock_locality_dic[addr_phy_addr].distant+1), (lock_locality_dic[addr_phy_addr].local))
            else:
                lock = LockLoc(1,0)
            lock_locality_dic[addr_phy_addr] = lock
        else:
            req_local_cluster += 1
            if addr_phy_addr in lock_locality_dic:
                lock = LockLoc((lock_locality_dic[addr_phy_addr].distant), (lock_locality_dic[addr_phy_addr].local+1))
            else:
                lock = LockLoc(0,1)
            lock_locality_dic[addr_phy_addr] = lock
            
    print "network_delays median : ", np.median(network_delays)
    print "L2_delays median : ", np.median(L2_delays), "nb echantillons ", len(L2_delays)
    
    plt.figure(1)
    plt.clf()
    plt.subplot(111)
    plt.boxplot(network_delays)
    plt.grid(True)
    plt.title('network delays')
    #plt.xlabel('core', fontsize=14, color='black')
    plt.ylabel('nb cycles', fontsize=14, color='black')
    
    plt.subplots_adjust(bottom = 0.2, hspace=2)
    plt.savefig('network_delays.png')
    subprocess.Popen(["display", "network_delays.png"])

    for addr,value in lock_locality_dic.items():
        print "lock @ : ", addr
        print "distant access : ", value.distant
        print "local access : ", value.local

    print "Total req processed : ", (req_local_cluster + req_distant_cluster)
    print "req local cluster : ", req_local_cluster
    print "req distan cluster : ", req_distant_cluster
