import sys
import numpy as np
import collections
import os
import subprocess
import matplotlib
import matplotlib.pyplot as plt

PRINT_DEBUG = False
NB_CLUSTERS = 4

LockLoc = collections.namedtuple('LockLoc', 'current_chain last_cluster host_cluster')
ChainReuse = collections.namedtuple('ChainReuse', 'local distant')

if __name__ == "__main__":
    delays_file = open("L2_delays.txt", 'r')

    outfile = open("Locality_measurments.txt", 'w')
    
    lock_locality_dic = {}
    clusters_reuse = []
    for i in range (0, NB_CLUSTERS):
        chain = ChainReuse({},{})
        clusters_reuse.append(chain)

    while(1):
        line = delays_file.readline()
        
        if (len(line) == 0):
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 9):
            print "error : line " + line + " not valid"
            continue

        phy_addr = int(fields[2],10)
        init_cluster = int(fields[7],10)
        dest_cluster = int(fields[8],10)

        if ((phy_addr in lock_locality_dic) and (init_cluster ==  lock_locality_dic[phy_addr].last_cluster)):
            lock = LockLoc((lock_locality_dic[phy_addr].current_chain+1), init_cluster, dest_cluster)
        elif ((phy_addr in lock_locality_dic) and (init_cluster !=  lock_locality_dic[phy_addr].last_cluster)):
            lock = LockLoc(0, init_cluster, dest_cluster)
            if (lock_locality_dic[phy_addr].last_cluster != lock_locality_dic[phy_addr].host_cluster):
                if lock_locality_dic[phy_addr].current_chain in clusters_reuse[lock_locality_dic[phy_addr].last_cluster].distant:
                    clusters_reuse[lock_locality_dic[phy_addr].last_cluster].distant[lock_locality_dic[phy_addr].current_chain] += 1
                else:
                    clusters_reuse[lock_locality_dic[phy_addr].last_cluster].distant[lock_locality_dic[phy_addr].current_chain] = 1
            else:
                if lock_locality_dic[phy_addr].current_chain in clusters_reuse[lock_locality_dic[phy_addr].last_cluster].local:
                    clusters_reuse[lock_locality_dic[phy_addr].last_cluster].local[lock_locality_dic[phy_addr].current_chain] += 1
                else:
                    clusters_reuse[lock_locality_dic[phy_addr].last_cluster].local[lock_locality_dic[phy_addr].current_chain] = 1
        else:
            lock = LockLoc(0, init_cluster, dest_cluster)

        lock_locality_dic[phy_addr] = lock


    print clusters_reuse
    reuse_nb = 0
    total_nb = 0

    total_local_chain_value = []
    total_distant_chain_value = []
    total_chain_length = []
    for i in range(0, len(clusters_reuse)):
        local_chain_value = []
        distant_chain_value = []
        chain_length = []

        line = "\ncluster "+ str(i)+" : \n"
        print line
        outfile.write(line)
        print "local accesses :"
        outfile.write("local accesses :\n")
        local_keylist = clusters_reuse[i].local.keys()
        local_keylist.sort()
        for index in local_keylist:
            value = clusters_reuse[i].local[index]
            line = "reuse chain length : " + str(index) + " / nb of chains : " + str(value) +"\n"
            print line
            outfile.write(line)
            total_nb += value*(index+1)
            if (index > 0):
                reuse_nb += value*index
        max_index = index

        print "distant accesses : \n"
        outfile.write("distant accesses :\n")
        distant_keylist = clusters_reuse[i].distant.keys()
        distant_keylist.sort()
        for index in distant_keylist:
            value = clusters_reuse[i].distant[index]
            line = "reuse chain length : " + str(index) + " / nb of chains : " + str(value)+"\n"
            print line
            outfile.write(line)
            total_nb += value*(index+1)
            if (index > 0):
                reuse_nb += value*index

        if (index > max_index):
            max_index = index

#        for j in range(0,max_index+1):
#            chain_length.append(j)
#            total_chain_length.append(j)
#            if j in clusters_reuse[i].local:
#                for k in range (0,clusters_reuse[i].local[j]):
#                    local_chain_value.append(j)
#                    total_local_chain_value.append(j)
                
#            if j in clusters_reuse[i].distant:
#                 for k in range (0,clusters_reuse[i].distant[j]):
#                     distant_chain_value.append(j)
#                     total_distant_chain_value.append(j)

        for j in range(0,max_index+1):
            chain_length.append(j)
            if j not in total_chain_length:
                total_chain_length.append(j)
                
            if j in clusters_reuse[i].local:
                local_chain_value.append(clusters_reuse[i].local[j])
            else :
                local_chain_value.append(0)
                
            if j in clusters_reuse[i].distant:
                distant_chain_value.append(clusters_reuse[i].distant[j])
            else:
                distant_chain_value.append(0)

        #Stack the data
        plt.figure(i)
        plt.clf()
        plt.subplot(111)
        #plt.hist([local_chain_value, distant_chain_value], chain_length, stacked=True, histtype='bar', label=['Local reuse', 'Distant reuse'])
        plt.bar(chain_length, local_chain_value, color = 'b', label='local reuse')
        plt.bar(chain_length, distant_chain_value, color = 'r', bottom = local_chain_value, label='Distant reuse')
        plt.legend()
        plt.yscale('log')
        line = "cluster " + str(i) + " reuse chain length"
        plt.title(line)
        plt.ylabel('nb of chain', fontsize=14, color='black')
        plt.xlabel('chain length', fontsize=14, color='black')
        line = "cluster_"+str(i)+"reuse.png"
        plt.savefig(line)
        subprocess.Popen(["display", line])


    print "total_chain_length :", total_chain_length
    for i in total_chain_length:
        sum_local = 0
        sum_distant = 0
        for cluster in range(0, len(clusters_reuse)):
            if i in clusters_reuse[cluster].local:
                sum_local += clusters_reuse[cluster].local[i]
            if i in clusters_reuse[cluster].distant:
                sum_distant += clusters_reuse[cluster].distant[i] 
        total_local_chain_value.append(sum_local)
        total_distant_chain_value.append(sum_distant)
        
    print "total_local_chain_value : ", total_local_chain_value
    print "total_distant_chain_value : ", total_distant_chain_value

    #global display
    plt.figure(i)
    plt.clf()
    plt.subplot(111)
    print "total_local_chain_value: ", total_local_chain_value
    print "total_distant_chain_value: ", total_distant_chain_value
    #plt.hist([total_local_chain_value, total_distant_chain_value], total_chain_length, stacked=True, label=['Local reuse', 'Distant reuse'])
    plt.bar(total_chain_length, total_local_chain_value, color = 'b', label='local reuse')
    plt.bar(total_chain_length, total_distant_chain_value, color = 'r', bottom = total_local_chain_value, label='Distant reuse')
    plt.legend()
    plt.yscale('log')
    line = "global reuse chain length"
    plt.title(line)
    plt.ylabel('nb of chain', fontsize=14, color='black')
    plt.xlabel('chain length', fontsize=14, color='black')
    line = "global_reuse.png"
    plt.savefig(line)
    subprocess.Popen(["display", line])

    line = "\nreuse : " + str(reuse_nb) + "\n"
    print line
    outfile.write(line)
    line = "total_nb : " + str(total_nb) + "\n"
    print line
    outfile.write(line)
    line = "ratio : " + str((reuse_nb*100)/total_nb) + "%\n" 
    print line
    outfile.write(line)
    
