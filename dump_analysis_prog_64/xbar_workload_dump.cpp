#include <iostream>
#include <stdio.h>

#include "structs.h"
#include "svdpi.h"
#include "tbxbindings.h"

/* shared variables */
extern volatile xbar_workload_t * xbar_workload;
extern uint8_t core_number;

int dump_xbar_workload(const svBitVecVal* xbar_req, unsigned int clock, unsigned int cluster_id)
{
	if (cluster_id < (core_number/4))
	{
		xbar_workload[cluster_id].xbar_req = (uint32_t) * xbar_req;
		xbar_workload[cluster_id].clock = clock;
		xbar_workload[cluster_id].cluster_id = cluster_id;
		printf("[WORKLOAD] proc %d => clock %d : in_time %d \n", cluster_id, clock, xbar_workload[cluster_id].xbar_req);
	}
	return 1;
}
