#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <string.h>

#include "structs.h"

#define DUMP_FILE_PATH "dump_signals.txt"

#ifdef DEBUG
#define debug_print(args ...) printf(args)
#else
#define debug_print(args ...)
#endif

/* shared variables */
extern volatile core_signals_t * data_signals;
extern std::vector <struct addresses> addresses;
extern uint8_t core_number;

/* global variables */
extern volatile bool exit_flag; //ending request

/* dump files */
static std::ofstream dump_file;


bool key_exist(std::map<uint32_t, signed_data_t> m, uint32_t key)
{
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool key_exist_uint32(std::map<uint32_t, uint32_t> m, uint32_t key)
{
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool key_exist_vector_item(std::map<uint32_t, std::vector<signed_data_t> > m, uint32_t key)
{	
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool key_exist_lock(std::map<uint32_t, locks_info_t> m, uint32_t key)
{
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool signed_item_exist_in_vector(std::vector<signed_data_t> v, signed_data_t item)
{
	for (std::vector<signed_data_t>::iterator it = v.begin() ; it != v.end(); ++it)
	{
		if ((it->tid.gp == item.tid.gp) && (it->tid.fp == item.tid.fp) && (it->data == item.data))
			return true;
	}
	return false;
}

/* create text files and write the headers of the dump signal in a file procX_signals.txt*/
void dumpfile_headers()
{
  dump_file.open(DUMP_FILE_PATH);
  dump_file << "#clock;proc;pc;r6;r28;r29;r31\n";
}

/* write dump signal in a file procX_signals.txt */
void write_signals(core_signals_t &proc_signals, uint8_t proc)
{
  if (!dump_file.is_open())
    dumpfile_headers();

  dump_file << proc_signals.clock << ";"
	    << (uint32_t)proc << ";" 
	     << proc_signals.pc << ";" 
	     << proc_signals.r6 << ";"
	     << proc_signals.r28 << ";"
	     << proc_signals.r29 << ";"
	     << proc_signals.r31 << "\n"; 
}

void * thread_analyse(void *arg)
{
	uint8_t program_current_index = 0;
	uint8_t proc;
	/* current_locks => key : @ lock  /  data : lock info */
	std::map <uint32_t, locks_info_t> locks_info;
	/* current_locks => key : fp  /  data : vector of lock */
	std::map <uint32_t, std::vector<signed_data_t> > current_locks;
	/* mutex_in_times => key : fp  / data : time */
	std::map <uint32_t, signed_data_t> mutex_in_times; // map is used to save time during search phases
	/* ret_addresses => key : fp / data : @ return */
	std::map <uint32_t, signed_data_t> ret_addresses; // map is used to save time during search phases
	/* sleeping_threads => key : fp / data : none */
	std::map <uint32_t, signed_data_t> sleeping_threads; // map is used to save time during search phases
	/* release_times => key : @ lock / data : time */
	std::map <uint32_t, uint32_t> release_times; // map is used to save time during search phases
	signed_data_t timestamp;
	signed_data_t return_addr;
	delays_t * delays = new delays_t [core_number];
	std::vector<uint32_t> prog_times;
	//first prog start at 0
	prog_times.push_back(0);
	// time of the last arrival of a thread/process to a barrier
	uint32_t last_arrival = 0;
	uint32_t aware_time = 0;
	uint32_t first_release = 0;
	uint8_t barrier_process_number = 0;
	uint32_t * old_clocks = (uint32_t *)malloc(core_number*sizeof(uint32_t));
	memset(old_clocks, 0, core_number*sizeof(uint32_t));
	// local copies of the shared mem
	core_signals_t * l_data_signals = new core_signals_t [core_number];

	printf("thread analyse, core number %d\n", core_number);

	//initialize the set of addresses of the first exe
	//load_addresses(program_current_index);
	
	// create and open dump files
	dumpfile_headers();

	while(!exit_flag)
	{
		/* we work on local copy of share mem */
		//memcpy((core_signals_t*)l_data_signals, (core_signals_t*)data_signals, core_number*sizeof(core_signals_t));
		
		for(proc=0; proc<core_number; proc++)
		{
		  if(data_signals[proc].clock == old_clocks[proc])
			continue;
		  /* we work on local copy of share mem */
		  memcpy((core_signals_t*)&l_data_signals[proc], (core_signals_t*)&data_signals[proc], sizeof(core_signals_t));

		  // update old clock value
		  old_clocks[proc] = l_data_signals[proc].clock;

		  // pc can't be null during executing flow
		  if(l_data_signals[proc].pc == 0)
		    continue;

		  // write signals in files
		  write_signals(l_data_signals[proc], proc);

		  //debug_print("! analyse loop clock %d (proc %d)!\n", l_data_signals[proc].clock, proc);

			/* start of a new program execution */
		  if (((program_current_index+1)<addresses.size()) && 
		      (l_data_signals[proc].pc == addresses[program_current_index+1].main_start_addr))
			{
				prog_times.push_back(l_data_signals[proc].clock);
				program_current_index++;
				debug_print("Analysis : change program new index %d \n",  program_current_index);
				
				/* exit of the loop when all prog have been proceeed */
				if (program_current_index>=addresses.size())
				{
					exit_flag = true;
					break;
				}else
				  {
				    // load exe addresses on dedicated hardware register in HDL
					  printf("!!! need to refresh addresses !!!\n");
				    //load_addresses(program_current_index);
				  }
			}
			/***********************************************************************************************/
			/*                                       lock analysis                                         */
			/***********************************************************************************************/
			/* enter in the lock function */
			if (l_data_signals[proc].pc == addresses[program_current_index].mutex_lock_addr)
			{
			  debug_print("Analysis : %X==%X => enter in lock function (proc %d)\n", l_data_signals[proc].pc, addresses[program_current_index].mutex_lock_addr, proc);
			
				timestamp.tid.gp = l_data_signals[proc].r28;
				timestamp.tid.fp = l_data_signals[proc].r29;
				timestamp.data = l_data_signals[proc].clock;
				mutex_in_times[timestamp.tid.fp] = timestamp;

				return_addr.tid = timestamp.tid;
				return_addr.data = l_data_signals[proc].r31;
				ret_addresses[return_addr.tid.fp] = return_addr;
			}
			// return from le lock function
			else if ((key_exist(ret_addresses,  l_data_signals[proc].r29)) &&  
				 (ret_addresses[l_data_signals[proc].r29].data == l_data_signals[proc].pc) &&
				 (ret_addresses[l_data_signals[proc].r29].tid.gp == l_data_signals[proc].r28))
			{
			  debug_print("Analysis : %X==%X => return from lock function (proc %d)\n", ret_addresses[l_data_signals[proc].r29].data, l_data_signals[proc].pc, proc);
			
				/* clear return address entry since we are not anymore in the function */
				ret_addresses.erase(l_data_signals[proc].r29);

				/* release delays compute */
				if ((key_exist_vector_item(current_locks, l_data_signals[proc].r29)) &&
				    (key_exist_uint32(release_times, current_locks[l_data_signals[proc].r29].back().data)))
				{
					debug_print("Analysis : release delay compute\n");
					/* does the threads go through waiting (sleeping process) ? */
					if (key_exist(sleeping_threads, l_data_signals[proc].r29))
					{
						debug_print("Analysis : previously sleeping %X\n", l_data_signals[proc].r29);
						// delete map entry
						sleeping_threads.erase(l_data_signals[proc].r29);
						delays[proc].lock.contented_release_delays.push_back(l_data_signals[proc].clock - release_times[current_locks[l_data_signals[proc].r29].back().data]);
					}
					else
					{
						delays[proc].lock.interlock_delays.push_back(l_data_signals[proc].clock - release_times[current_locks[l_data_signals[proc].r29].back().data]);
					}
					// delete the entry from the map
					release_times.erase(current_locks[l_data_signals[proc].r29].back().data);
				}
		
				/* compute acquire delay */
				// retrive in function timestamps
				if ((key_exist(mutex_in_times,  l_data_signals[proc].r29)) && (mutex_in_times[l_data_signals[proc].r29].tid.gp == l_data_signals[proc].r28))
				{
					debug_print("Analysis : acquire delay compute\n");
					delays[proc].lock.acquire_delays.push_back(l_data_signals[proc].clock - mutex_in_times[l_data_signals[proc].r29].data);
					// clear in function timestamps entry of the map
					mutex_in_times.erase(l_data_signals[proc].r29);
				}
			}
			// go to sleep
			else if ((l_data_signals[proc].pc == addresses[program_current_index].lock_wait_addr) ||
				 (l_data_signals[proc].pc == addresses[program_current_index].lock_wait_priv_addr))
			{
			  debug_print("Analysis : %X==%X/%X go to sleep (proc %d) \n", l_data_signals[proc].pc, addresses[program_current_index].lock_wait_addr, addresses[program_current_index].lock_wait_priv_addr, proc);
				/* clear return address entry since we are not anymore in the function */
				if (key_exist(ret_addresses,  l_data_signals[proc].r29))
					ret_addresses.erase(l_data_signals[proc].r29);
				/* compute go to sleep delay */
				// retrieve in function timestamps
				if ((key_exist(mutex_in_times,  l_data_signals[proc].r29)) && (mutex_in_times[l_data_signals[proc].r29].tid.gp == l_data_signals[proc].r28))
				{
					debug_print("Analysis : go to sleep delay computation \n");
					delays[proc].lock.go_to_sleep_delays.push_back(l_data_signals[proc].clock - mutex_in_times[l_data_signals[proc].r29].data);
					// clear in function timestamps entry of the map
					mutex_in_times.erase(l_data_signals[proc].r29);
				}

				// store thread id in sleeping thread map
				signed_data_t tid;
				tid.tid.gp = l_data_signals[proc].r28;
				tid.tid.fp = l_data_signals[proc].r29;
				sleeping_threads[tid.tid.fp] = tid;
			}
			// critical section duration
			else if (l_data_signals[proc].pc == addresses[program_current_index].mutex_unlock_addr)
			{
			  debug_print("Analysis : %X==%X critical section (proc %d) \n", l_data_signals[proc].pc, addresses[program_current_index].mutex_unlock_addr, proc);
				/* clear return address entry since we are not anymore in the function */
				if (key_exist(ret_addresses,  l_data_signals[proc].r29))
					ret_addresses.erase(l_data_signals[proc].r29);
				/* compute critical section duration */
				// retrive in function timestamps
				if ((key_exist(mutex_in_times,  l_data_signals[proc].r29)) && (mutex_in_times[l_data_signals[proc].r29].tid.gp == l_data_signals[proc].r28))
				{
					debug_print("Analysis : critical section computation \n");
					delays[proc].lock.cs_times.push_back(l_data_signals[proc].clock - mutex_in_times[l_data_signals[proc].r29].data);
					// clear in function timestamps entry of the map
					mutex_in_times.erase(l_data_signals[proc].r29);
				}

				// save release time
				if (key_exist_vector_item(current_locks, l_data_signals[proc].r29))
				{
					debug_print("Analysis : save release time for fp %d \n", l_data_signals[proc].r29);
					release_times[current_locks[l_data_signals[proc].r29].back().data] = l_data_signals[proc].clock;
					// get the current lock entry
					// we assume that in nested lock the first lock to be released is the last acquire 
					current_locks[l_data_signals[proc].r29].pop_back();
					if (current_locks[l_data_signals[proc].r29].size() == 0)
						current_locks.erase(l_data_signals[proc].r29);
				}   
			}
			// lock identification
			else if (l_data_signals[proc].pc == addresses[program_current_index].ll_instr_addr)
			{
			  debug_print("Analysis : %X==%X lock identification (proc %d) \n", l_data_signals[proc].pc, addresses[program_current_index].ll_instr_addr, proc);
				// we implement a vector of current lock form each tid to manage nested lock
				signed_data_t lock;
				lock.tid.gp = l_data_signals[proc].r28;
				lock.tid.fp = l_data_signals[proc].r29;
				lock.data = l_data_signals[proc].r6;
				// the ll instruction could be in a loop is case of cs instruction failed
				// so we have to prevent from multiple adding
				if ((!key_exist_vector_item(current_locks, l_data_signals[proc].r29)) &&
				    (!signed_item_exist_in_vector(current_locks[l_data_signals[proc].r29], lock)))
				{
					debug_print("Analysis : add current lock %X to fp %X\n", lock.data, lock.tid.fp);
					current_locks[l_data_signals[proc].r29].push_back(lock);

					if (key_exist_lock(locks_info, l_data_signals[proc].r6))
					{
						locks_info[l_data_signals[proc].r6].contention++;
						debug_print("Analysis : add current lock %X contention %d\n", lock.data, locks_info[l_data_signals[proc].r6].contention);
					}
					else
					{
						locks_info_t lockinfo;
						lockinfo.contention = 1;
						locks_info[l_data_signals[proc].r6] = lockinfo;
						debug_print("Analysis : [new] add current lock %X contention %d\n", lock.data, locks_info[l_data_signals[proc].r6].contention);
					}
					struct lock_dyn l_dyn;
					l_dyn.time_req = l_data_signals[proc].clock;
					l_dyn.proc = proc;
					locks_info[l_data_signals[proc].r6].dyn.push_back(l_dyn);
				}
			}
			/***********************************************************************************************/
			/*                                                barrier analysis                                              */
			/***********************************************************************************************/
			/* we assume that only one barrier take place in a time */
			// process arrival
			else if (l_data_signals[proc].pc == addresses[program_current_index].barrier_addr)
			{
			  debug_print("Analysis : %X==%X => barrier arrival (proc %d) \n", l_data_signals[proc].pc, addresses[program_current_index].barrier_addr, proc);
				delays[proc].barrier.arrival_times.push_back(l_data_signals[proc].clock);
				last_arrival = l_data_signals[proc].clock;
				// reinit the first release variable for the next barrier
				first_release = 0;
				// count the number of processes/threads take part in the barrier
				barrier_process_number++;
			}
			// master aware
			else if (l_data_signals[proc].pc == addresses[program_current_index].barrier_instr_futex_wake_addr)
			{
			  debug_print("Analysis : %X==%X => master aware time %d (proc %d) \n", l_data_signals[proc].pc,  addresses[program_current_index].barrier_instr_futex_wake_addr, l_data_signals[proc].clock, proc);
				delays[proc].barrier.aware_delays.push_back(l_data_signals[proc].clock-last_arrival);
				aware_time = l_data_signals[proc].clock;
			}
			else if (l_data_signals[proc].pc == addresses[program_current_index].barrier_wait_end_addr)
			{
			  debug_print("Analysis : %X==%X => release  (proc %d) \n", l_data_signals[proc].pc,  addresses[program_current_index].barrier_wait_end_addr, proc);
				if (first_release == 0)
				{
					debug_print("Analysis :  first release  %d \n", l_data_signals[proc].clock);
					first_release = l_data_signals[proc].clock;
					delays[proc].barrier.aware_2_first_release_delays.push_back(l_data_signals[proc].clock-aware_time);
				}
				barrier_process_number--;
				// last release process / thread
				if (barrier_process_number == 0)
				{
					debug_print("Analysis : last release  %d \n", l_data_signals[proc].clock);
					delays[proc].barrier.aware_2_last_release_delays.push_back(l_data_signals[proc].clock-aware_time);
					delays[proc].barrier.arrival_2_last_release_delays.push_back(l_data_signals[proc].clock-last_arrival);
				}
			}
		}
	}

	debug_print("generate output file...\n");
	/* generate output file */
	std::ofstream myfile;
	uint8_t i;
	myfile.open ("delays.txt");
	myfile << "File containing all computed delays\n";
	myfile << "programs times;";
	for(i=0; i<prog_times.size();i++)
	{
		myfile << prog_times[i] << ";";
	}
	myfile << "\nlock delays\n";
	myfile << "acquire delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].lock.acquire_delays.size();i++)
			myfile << delays[proc].lock.acquire_delays[i] << ";";
	}
	myfile << "\ncritical section delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].lock.cs_times.size();i++)
			myfile << delays[proc].lock.cs_times[i] << ";";
	}
	myfile << "\ngo to sleep delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].lock.go_to_sleep_delays.size();i++)
			myfile << delays[proc].lock.go_to_sleep_delays[i] << ";";
	}
	myfile << "\ncontented release delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].lock.contented_release_delays.size();i++)
			myfile << delays[proc].lock.contented_release_delays[i] << ";";
	}
	myfile << "\ninterlock delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].lock.interlock_delays.size();i++)
			myfile << delays[proc].lock.interlock_delays[i] << ";";
	}
	myfile << "\nlock informations\n";
	myfile << "lock contention\n";
	for (std::map <uint32_t, locks_info_t>::iterator it=locks_info.begin(); it!=locks_info.end(); ++it)
		myfile << it->first << ";" << it->second.contention << "\n";
	myfile << "lock dynamic ( @lock;time_1(procX);time_2(procY);... )\n";
	for (std::map <uint32_t, locks_info_t>::iterator it=locks_info.begin(); it!=locks_info.end(); ++it)
	{
		myfile << it->first << ";";
		for (i=0; i<it->second.dyn.size();i++)
			myfile << it->second.dyn[i].time_req << "(" << it->second.dyn[i].proc << ");";
	}
	myfile << "\nbarrier delays\n";
	myfile << "arrival times";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].barrier.arrival_times.size();i++)
			myfile << delays[proc].barrier.arrival_times[i] << ";";
	}
	myfile << "\nwakeup times";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].barrier.wakeup_times.size();i++)
			myfile << delays[proc].barrier.wakeup_times[i] << ";";
	}
	myfile << "\naware delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].barrier.aware_delays.size();i++)
			myfile << delays[proc].barrier.aware_delays[i] << ";";
	}
	myfile << "\naware to first release delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].barrier.aware_2_first_release_delays.size();i++)
			myfile << delays[proc].barrier.aware_2_first_release_delays[i] << ";";
	}
	myfile << "\naware to last release delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].barrier.aware_2_last_release_delays.size();i++)
			myfile << delays[proc].barrier.aware_2_last_release_delays[i] << ";";
	}
	myfile << "\narrival to last release delays";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\nproc" <<(uint32_t)proc<<";";
		for (i=0; i<delays[proc].barrier.arrival_2_last_release_delays.size();i++)
			myfile << delays[proc].barrier.arrival_2_last_release_delays[i] << ";";
	}
	
	myfile.close();

	// close dump files
	dump_file.close();

	delete [] delays;
	delete [] l_data_signals;
	free(old_clocks);
	
	return NULL;
}
