module dump_signals_wrapper ();
   // pragma attribute dump_signals_wrapper partition_module_xrtl
   
   wire clk;
   wire clk_bis;
   
   wire [63:0][31:0] s_pc_vector;
   wire [63:0][31:0] s_r6_vector;
   wire [63:0][31:0] s_r28_vector;
   wire [63:0][31:0] s_r29_vector;
   wire [63:0][31:0] s_r31_vector;
   wire [63:0][31:0] s_instr_vector;


   reg[31:0] mutex_lock_addr;
   reg[31:0] mutex_unlock_addr;
   reg[31:0] lock_wait_addr;
   reg[31:0] lock_wait_priv_addr;
   reg[31:0] barrier_addr;
   reg[31:0] barrier_wait_end_addr;
   reg[31:0] barrier_instr_futex_wake_addr;
   reg[31:0] main_start_addr;
   reg[31:0] return_main_addr;
   reg[31:0] ll_instr_addr;
   reg[31:0] ll_instr_addr_2;
   reg[31:0] futex_wake_addr;
   reg [31:0] fct_uname_1_addr;
   reg [31:0] fct_uname_2_addr;
   reg [31:0] fct_uname_3_addr;
   reg [31:0] start_interest_section_addr;
   reg [31:0] end_interest_section_addr;
   reg [31:0] detailled_interest_section;

   reg [31:0] trig_instr_1;
   reg [31:0] mask_trig_instr_1;
   reg [31:0] trig_instr_2;
   reg [31:0] mask_trig_instr_2;
   int unsigned next_instr_trig[63:0];

   reg [63:0][31:0] r31_bank;
   reg [63:0][31:0] ret_futex_wake_bank;
   reg [63:0][31:0] ret_uname_bank_1;
   reg [63:0][31:0] ret_uname_bank_2;
   reg [63:0][31:0] ret_uname_bank_3;

   reg [63:0][31:0] prev_pc_bank;

   wire [63:0][1:0] L1_vci_cmd;
   wire [63:0][39:0] L1_s_phy_addr; // physical address on 40 bits
   wire [63:0] L1_cmdval;
   wire [63:0][13:0] L1_cmd_srcid;
   wire [63:0][3:0] L1_cmd_trdid;
   wire [63:0][3:0] L1_cmd_pktid;

   reg [63:0][13:0] triggered_srcid;
   reg [63:0][3:0] triggered_trdid;
   reg [63:0][3:0] triggered_pktid;

   reg [63:0][13:0] prev_L2_cmd_srcid;
   reg [63:0][3:0] prev_L2_cmd_trdid;
   reg [63:0][3:0] prev_L2_cmd_pktid;

   wire [15:0][1:0] L2_vci_cmd;
   wire [15:0][39:0] L2_s_phy_addr; // physical address on 40 bits
   wire [15:0] L2_cmdval;
   wire [15:0] L2_cmdack;
   wire [15:0] L2_vci_tgt_rspval;
   wire [15:0][3:0] L2_cmd_trdid;
   wire [15:0][3:0] L2_cmd_pktid;
   wire [15:0][13:0] L2_cmd_srcid;
   wire [15:0][3:0] L2_rsp_trdid;
   wire [15:0][3:0] L2_rsp_pktid;
   wire [15:0][13:0] L2_rsp_rsrcid;

   wire [15:0][5:0]  xbar_req;
   wire [15:0] xbar_rel;
   wire [15:0] xbar_r_allocated;
   
   reg [15:0][63:0] L2_cmd_in_time;

   reg [63:0][39:0] prev_L1_phy_addr; // physical address on 40 bits
   reg [15:0][39:0] prev_L2_phy_addr; // physical address on 40 bits
   
   longint unsigned clock;
   longint unsigned clock_bis;
   int unsigned i;
   int unsigned addr_loaded = 0;
   int unsigned in_main_prog = 0;
   int unsigned interest_function[63:0];
   int unsigned interest_section_core = 0;

   int unsigned global_interest_section = 0;
   int unsigned interest_section[63:0];

   int unsigned ll_sc_pending[63:0];
   
   initial begin
      for(int i=0; i<64; i++) begin
	 interest_function[i] = 0;
	 next_instr_trig[i] = 0;
	 interest_section[i] = 0;
	 ll_sc_pending[i] = 0;
      end
   end
   
   // Declare C functions used here and verilog functions
   // used in the C comodel
   import "DPI-C" task load_trig_addresses(output bit [31:0] mutex_lock_addr_out, output bit [31:0] mutex_unlock_addr_out, output bit [31:0] lock_wait_addr_out, output bit [31:0] lock_wait_priv_addr_out, output bit [31:0] barrier_addr_out, output bit [31:0] barrier_wait_end_addr_out, output bit [31:0] barrier_instr_futex_wake_addr_out, output bit [31:0] main_start_addr_out, output bit [31:0] ll_instr_addr_out, output bit [31:0] ll_instr_addr_2_out, output bit [31:0] futex_wake_addr,  output bit [31:0] fct_uname_1_addr, output bit [31:0] fct_uname_2_addr, output bit [31:0] fct_uname_3_addr, output bit [31:0] start_interest_section_addr, output bit [31:0] end_interest_section_addr, output bit [31:0] return_main_addr, output bit [31:0] trig_instr_1, output bit [31:0] mask_trig_instr_1, output bit [31:0] trig_instr_2, output bit [31:0] mask_trig_instr_2, output bit [31:0] detailled_interest_section);
   import "DPI-C" task dump_signals(input bit[31:0] pc, input bit[31:0] instr, input bit[31:0] r6, input bit[31:0] r28, input bit[31:0] r29, input bit[31:0] r31, input bit[63:0] clock, input bit[63:0] clock_bis, input int unsigned proc_id);


   import "DPI-C" task dump_L1_vci(input bit[31:0] pc, input bit[31:0] instr, input bit[1:0] L1_vci_cmd, input bit[39:0] phy_addr, input bit[13:0] srcid, input bit[3:0] trdid, input bit[3:0] pktid, input int unsigned clock, input int unsigned core_id);
   import "DPI-C" task dump_L2_vci(input bit[1:0] L2_vci_cmd, input bit[39:0] phy_addr, input bit[13:0] srcid, input bit[3:0] trdid, input bit[3:0] pktid, input int unsigned clock, input int unsigned cluster_id);

   import "DPI-C" task dump_L2_workload(input bit[31:0] L2_cmd_in_time, input int unsigned clock, input int unsigned cluster_id);
   import "DPI-C" task dump_xbar_workload(input bit[31:0] L2_cmd_in_time, input int unsigned clock, input int unsigned cluster_id);
				      
   assign clk = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[0].GENERATE_Y_CLUSTER[0].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[0].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.CK;
   assign clk_bis = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[0].GENERATE_Y_CLUSTER[1].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[0].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.CK;

   assign reset_n = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[0].GENERATE_Y_CLUSTER[0].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[0].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.RESET_N;
   
   generate
      genvar assign_var;
      for (assign_var=0; assign_var < 64; assign_var++)  begin: module_assign
	 assign s_pc_vector[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.PC_RE;
	 assign s_instr_vector[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.I_RE;
	 assign s_r6_vector[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.R6_RW;
	 assign s_r28_vector[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.R28_RW;
	 assign s_r29_vector[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.R29_RW; // initially R30_RW, but we try to store the sp in place of fp to solve some thread identification issues
	 assign s_r31_vector[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.mips_32_sync_wrapper_inst.mips_32_inst.R31_RW;

	 assign L1_s_phy_addr[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.vci_cc_vcache_sync_inst.vci_ini_p_address;
	 assign L1_vci_cmd[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.vci_cc_vcache_sync_inst.vci_ini_p_cmd;
	 assign L1_cmdval[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.vci_cc_vcache_sync_inst.vci_ini_p_cmdval;
	 assign L1_cmd_trdid[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.vci_cc_vcache_sync_inst.vci_ini_p_trdid;
	 assign L1_cmd_pktid[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.vci_cc_vcache_sync_inst.vci_ini_p_pktid;
	 assign L1_cmd_srcid[assign_var] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var[5:4]].GENERATE_Y_CLUSTER[assign_var[3:2]].tsarcluster_inst.unit_io_inst.core_inst.genL1Cache[assign_var[1:0]].vcache_inst.vci_cc_vcache_sync_inst.vci_ini_p_srcid;
      end
   endgenerate

    generate
      genvar assign_var2;
      for (assign_var2=0; assign_var2 < 16; assign_var2++)  begin: module_assign2
	 assign L2_s_phy_addr[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_address;
	 assign L2_vci_cmd[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_cmd;
	 assign L2_cmdval[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_cmdval;
	 assign L2_cmdack[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_cmdack;
	 assign L2_vci_tgt_rspval[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_rspval;
	 assign L2_cmd_trdid[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_trdid;
	 assign L2_cmd_pktid[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_pktid;
	  assign L2_cmd_srcid[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_srcid;
	 assign L2_rsp_trdid[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_rtrdid;
	 assign L2_rsp_pktid[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_rpktid;
	 assign L2_rsp_rsrcid[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_mem_cache_inst.p_vci_tgt_rsrcid;

	 assign xbar_rel[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_cmd_xbar_inst.genSwitchTgt[0].cmd_tgt.mux_sel_inst.rel;
	 assign xbar_req[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_cmd_xbar_inst.genSwitchTgt[0].cmd_tgt.mux_sel_inst.req;
	 assign xbar_r_allocated[assign_var2] = tsar_generic_mesh_inst.GENERATE_X_CLUSTER[assign_var2[3:2]].GENERATE_Y_CLUSTER[assign_var2[1:0]].tsarcluster_inst.unit_io_inst.core_inst.vci_cmd_xbar_inst.genSwitchTgt[0].cmd_tgt.mux_sel_inst.r_allocated;
      end
   endgenerate

   generate
      genvar ig;
      for (ig=0; ig < 64; ig++)  begin: module_gen
	 always @(posedge clk) 
	   begin
	      if (s_pc_vector[ig] != 0)
		begin
		   if (s_pc_vector[ig]==main_start_addr)
		     begin
			in_main_prog = 1;
			dump_signals(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, main_start_addr, ll_instr_addr, 2000, 2000, 110);
		     end
		   if (in_main_prog == 1)
		     begin
			/*
			if (s_pc_vector[ig]==mutex_lock_addr || s_pc_vector[ig]==mutex_unlock_addr || 
			    s_pc_vector[ig]==lock_wait_addr || s_pc_vector[ig]==lock_wait_priv_addr || 
			    s_pc_vector[ig]==barrier_addr || s_pc_vector[ig]==barrier_wait_end_addr || 
			    s_pc_vector[ig]==barrier_instr_futex_wake_addr || 
			    s_pc_vector[ig]==main_start_addr || 
			    s_pc_vector[ig]==ll_instr_addr || s_pc_vector[ig]==ll_instr_addr_2 ||
			    s_pc_vector[ig]==futex_wake_addr || s_pc_vector[ig]==fct_uname_1_addr ||
			    s_pc_vector[ig]==fct_uname_2_addr || s_pc_vector[ig]==fct_uname_3_addr ||
			    s_pc_vector[ig]==r31_bank[0] || s_pc_vector[ig]==r31_bank[1] || 
			    s_pc_vector[ig]==r31_bank[2] || s_pc_vector[ig]==r31_bank[3] ||
			    s_pc_vector[ig]==r31_bank[4] || s_pc_vector[ig]==r31_bank[5] || 
			    s_pc_vector[ig]==r31_bank[6] || s_pc_vector[ig]==r31_bank[7] ||
			    s_pc_vector[ig]==r31_bank[8] || s_pc_vector[ig]==r31_bank[9] || 
			    s_pc_vector[ig]==r31_bank[10] || s_pc_vector[ig]==r31_bank[11] ||
			    s_pc_vector[ig]==r31_bank[12] || s_pc_vector[ig]==r31_bank[13] || 
			    s_pc_vector[ig]==r31_bank[14] || s_pc_vector[ig]==r31_bank[15] ||
			    s_pc_vector[ig]==r31_bank[16] || s_pc_vector[ig]==r31_bank[17] || 
			    s_pc_vector[ig]==r31_bank[18] || s_pc_vector[ig]==r31_bank[19] ||
			    s_pc_vector[ig]==r31_bank[20] || s_pc_vector[ig]==r31_bank[21] || 
			    s_pc_vector[ig]==r31_bank[22] || s_pc_vector[ig]==r31_bank[23] ||
			    s_pc_vector[ig]==r31_bank[24] || s_pc_vector[ig]==r31_bank[25] || 
			    s_pc_vector[ig]==r31_bank[26] || s_pc_vector[ig]==r31_bank[27] ||
			    s_pc_vector[ig]==r31_bank[28] || s_pc_vector[ig]==r31_bank[29] || 
			    s_pc_vector[ig]==r31_bank[30] || s_pc_vector[ig]==r31_bank[31] ||
			    s_pc_vector[ig]==r31_bank[32] || s_pc_vector[ig]==r31_bank[33] || 
			    s_pc_vector[ig]==r31_bank[34] || s_pc_vector[ig]==r31_bank[35] ||
			    s_pc_vector[ig]==r31_bank[36] || s_pc_vector[ig]==r31_bank[37] || 
			    s_pc_vector[ig]==r31_bank[38] || s_pc_vector[ig]==r31_bank[39] ||
			    s_pc_vector[ig]==r31_bank[40] || s_pc_vector[ig]==r31_bank[41] || 
			    s_pc_vector[ig]==r31_bank[42] || s_pc_vector[ig]==r31_bank[43] ||
			    s_pc_vector[ig]==r31_bank[44] || s_pc_vector[ig]==r31_bank[45] || 
			    s_pc_vector[ig]==r31_bank[46] || s_pc_vector[ig]==r31_bank[47] ||
			    s_pc_vector[ig]==r31_bank[48] || s_pc_vector[ig]==r31_bank[49] || 
			    s_pc_vector[ig]==r31_bank[50] || s_pc_vector[ig]==r31_bank[51] ||
			    s_pc_vector[ig]==r31_bank[52] || s_pc_vector[ig]==r31_bank[53] || 
			    s_pc_vector[ig]==r31_bank[54] || s_pc_vector[ig]==r31_bank[55] ||
			    s_pc_vector[ig]==r31_bank[56] || s_pc_vector[ig]==r31_bank[57] || 
			    s_pc_vector[ig]==r31_bank[58] || s_pc_vector[ig]==r31_bank[59] ||
			    s_pc_vector[ig]==r31_bank[60] || s_pc_vector[ig]==r31_bank[61] || 
			    s_pc_vector[ig]==r31_bank[62] || s_pc_vector[ig]==r31_bank[63] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[0] || s_pc_vector[ig]==ret_futex_wake_bank[1] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[2] || s_pc_vector[ig]==ret_futex_wake_bank[3] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[4] || s_pc_vector[ig]==ret_futex_wake_bank[5] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[6] || s_pc_vector[ig]==ret_futex_wake_bank[7] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[8] || s_pc_vector[ig]==ret_futex_wake_bank[9] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[10] || s_pc_vector[ig]==ret_futex_wake_bank[11] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[12] || s_pc_vector[ig]==ret_futex_wake_bank[13] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[14] || s_pc_vector[ig]==ret_futex_wake_bank[15] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[16] || s_pc_vector[ig]==ret_futex_wake_bank[17] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[18] || s_pc_vector[ig]==ret_futex_wake_bank[19] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[20] || s_pc_vector[ig]==ret_futex_wake_bank[21] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[22] || s_pc_vector[ig]==ret_futex_wake_bank[23] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[24] || s_pc_vector[ig]==ret_futex_wake_bank[25] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[26] || s_pc_vector[ig]==ret_futex_wake_bank[27] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[28] || s_pc_vector[ig]==ret_futex_wake_bank[29] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[30] || s_pc_vector[ig]==ret_futex_wake_bank[31] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[32] || s_pc_vector[ig]==ret_futex_wake_bank[33] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[34] || s_pc_vector[ig]==ret_futex_wake_bank[35] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[36] || s_pc_vector[ig]==ret_futex_wake_bank[37] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[38] || s_pc_vector[ig]==ret_futex_wake_bank[39] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[40] || s_pc_vector[ig]==ret_futex_wake_bank[41] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[42] || s_pc_vector[ig]==ret_futex_wake_bank[43] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[44] || s_pc_vector[ig]==ret_futex_wake_bank[45] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[46] || s_pc_vector[ig]==ret_futex_wake_bank[47] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[48] || s_pc_vector[ig]==ret_futex_wake_bank[49] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[50] || s_pc_vector[ig]==ret_futex_wake_bank[51] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[52] || s_pc_vector[ig]==ret_futex_wake_bank[53] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[54] || s_pc_vector[ig]==ret_futex_wake_bank[55] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[56] || s_pc_vector[ig]==ret_futex_wake_bank[57] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[58] || s_pc_vector[ig]==ret_futex_wake_bank[59] ||
			    s_pc_vector[ig]==ret_futex_wake_bank[60] || s_pc_vector[ig]==ret_futex_wake_bank[61] || 
			    s_pc_vector[ig]==ret_futex_wake_bank[62] || s_pc_vector[ig]==ret_futex_wake_bank[63] ||
			    s_pc_vector[ig]==ret_uname_bank_1[0] || s_pc_vector[ig]==ret_uname_bank_1[1] || 
			    s_pc_vector[ig]==ret_uname_bank_1[2] || s_pc_vector[ig]==ret_uname_bank_1[3] ||
			    s_pc_vector[ig]==ret_uname_bank_1[4] || s_pc_vector[ig]==ret_uname_bank_1[5] || 
			    s_pc_vector[ig]==ret_uname_bank_1[6] || s_pc_vector[ig]==ret_uname_bank_1[7] ||
			    s_pc_vector[ig]==ret_uname_bank_1[8] || s_pc_vector[ig]==ret_uname_bank_1[9] || 
			    s_pc_vector[ig]==ret_uname_bank_1[10] || s_pc_vector[ig]==ret_uname_bank_1[11] ||
			    s_pc_vector[ig]==ret_uname_bank_1[12] || s_pc_vector[ig]==ret_uname_bank_1[13] || 
			    s_pc_vector[ig]==ret_uname_bank_1[14] || s_pc_vector[ig]==ret_uname_bank_1[15] ||
			    s_pc_vector[ig]==ret_uname_bank_1[16] || s_pc_vector[ig]==ret_uname_bank_1[17] || 
			    s_pc_vector[ig]==ret_uname_bank_1[18] || s_pc_vector[ig]==ret_uname_bank_1[19] ||
			    s_pc_vector[ig]==ret_uname_bank_1[20] || s_pc_vector[ig]==ret_uname_bank_1[21] || 
			    s_pc_vector[ig]==ret_uname_bank_1[22] || s_pc_vector[ig]==ret_uname_bank_1[23] ||
			    s_pc_vector[ig]==ret_uname_bank_1[24] || s_pc_vector[ig]==ret_uname_bank_1[25] || 
			    s_pc_vector[ig]==ret_uname_bank_1[26] || s_pc_vector[ig]==ret_uname_bank_1[27] ||
			    s_pc_vector[ig]==ret_uname_bank_1[28] || s_pc_vector[ig]==ret_uname_bank_1[29] || 
			    s_pc_vector[ig]==ret_uname_bank_1[30] || s_pc_vector[ig]==ret_uname_bank_1[31] ||
			    s_pc_vector[ig]==ret_uname_bank_1[32] || s_pc_vector[ig]==ret_uname_bank_1[33] || 
			    s_pc_vector[ig]==ret_uname_bank_1[34] || s_pc_vector[ig]==ret_uname_bank_1[35] ||
			    s_pc_vector[ig]==ret_uname_bank_1[36] || s_pc_vector[ig]==ret_uname_bank_1[37] || 
			    s_pc_vector[ig]==ret_uname_bank_1[38] || s_pc_vector[ig]==ret_uname_bank_1[39] ||
			    s_pc_vector[ig]==ret_uname_bank_1[40] || s_pc_vector[ig]==ret_uname_bank_1[41] || 
			    s_pc_vector[ig]==ret_uname_bank_1[42] || s_pc_vector[ig]==ret_uname_bank_1[43] ||
			    s_pc_vector[ig]==ret_uname_bank_1[44] || s_pc_vector[ig]==ret_uname_bank_1[45] || 
			    s_pc_vector[ig]==ret_uname_bank_1[46] || s_pc_vector[ig]==ret_uname_bank_1[47] ||
			    s_pc_vector[ig]==ret_uname_bank_1[48] || s_pc_vector[ig]==ret_uname_bank_1[49] || 
			    s_pc_vector[ig]==ret_uname_bank_1[50] || s_pc_vector[ig]==ret_uname_bank_1[51] ||
			    s_pc_vector[ig]==ret_uname_bank_1[52] || s_pc_vector[ig]==ret_uname_bank_1[53] || 
			    s_pc_vector[ig]==ret_uname_bank_1[54] || s_pc_vector[ig]==ret_uname_bank_1[55] ||
			    s_pc_vector[ig]==ret_uname_bank_1[56] || s_pc_vector[ig]==ret_uname_bank_1[57] || 
			    s_pc_vector[ig]==ret_uname_bank_1[58] || s_pc_vector[ig]==ret_uname_bank_1[59] ||
			    s_pc_vector[ig]==ret_uname_bank_1[60] || s_pc_vector[ig]==ret_uname_bank_1[61] || 
			    s_pc_vector[ig]==ret_uname_bank_1[62] || s_pc_vector[ig]==ret_uname_bank_1[63] ||
			    s_pc_vector[ig]==ret_uname_bank_2[0] || s_pc_vector[ig]==ret_uname_bank_2[1] || 
			    s_pc_vector[ig]==ret_uname_bank_2[2] || s_pc_vector[ig]==ret_uname_bank_2[3] ||
			    s_pc_vector[ig]==ret_uname_bank_2[4] || s_pc_vector[ig]==ret_uname_bank_2[5] || 
			    s_pc_vector[ig]==ret_uname_bank_2[6] || s_pc_vector[ig]==ret_uname_bank_2[7] ||
			    s_pc_vector[ig]==ret_uname_bank_2[8] || s_pc_vector[ig]==ret_uname_bank_2[9] || 
			    s_pc_vector[ig]==ret_uname_bank_2[10] || s_pc_vector[ig]==ret_uname_bank_2[11] ||
			    s_pc_vector[ig]==ret_uname_bank_2[12] || s_pc_vector[ig]==ret_uname_bank_2[13] || 
			    s_pc_vector[ig]==ret_uname_bank_2[14] || s_pc_vector[ig]==ret_uname_bank_2[15] ||
			    s_pc_vector[ig]==ret_uname_bank_2[16] || s_pc_vector[ig]==ret_uname_bank_2[17] || 
			    s_pc_vector[ig]==ret_uname_bank_2[18] || s_pc_vector[ig]==ret_uname_bank_2[19] ||
			    s_pc_vector[ig]==ret_uname_bank_2[20] || s_pc_vector[ig]==ret_uname_bank_2[21] || 
			    s_pc_vector[ig]==ret_uname_bank_2[22] || s_pc_vector[ig]==ret_uname_bank_2[23] ||
			    s_pc_vector[ig]==ret_uname_bank_2[24] || s_pc_vector[ig]==ret_uname_bank_2[25] || 
			    s_pc_vector[ig]==ret_uname_bank_2[26] || s_pc_vector[ig]==ret_uname_bank_2[27] ||
			    s_pc_vector[ig]==ret_uname_bank_2[28] || s_pc_vector[ig]==ret_uname_bank_2[29] || 
			    s_pc_vector[ig]==ret_uname_bank_2[30] || s_pc_vector[ig]==ret_uname_bank_2[31] ||
			    s_pc_vector[ig]==ret_uname_bank_2[32] || s_pc_vector[ig]==ret_uname_bank_2[33] || 
			    s_pc_vector[ig]==ret_uname_bank_2[34] || s_pc_vector[ig]==ret_uname_bank_2[35] ||
			    s_pc_vector[ig]==ret_uname_bank_2[36] || s_pc_vector[ig]==ret_uname_bank_2[37] || 
			    s_pc_vector[ig]==ret_uname_bank_2[38] || s_pc_vector[ig]==ret_uname_bank_2[39] ||
			    s_pc_vector[ig]==ret_uname_bank_2[40] || s_pc_vector[ig]==ret_uname_bank_2[41] || 
			    s_pc_vector[ig]==ret_uname_bank_2[42] || s_pc_vector[ig]==ret_uname_bank_2[43] ||
			    s_pc_vector[ig]==ret_uname_bank_2[44] || s_pc_vector[ig]==ret_uname_bank_2[45] || 
			    s_pc_vector[ig]==ret_uname_bank_2[46] || s_pc_vector[ig]==ret_uname_bank_2[47] ||
			    s_pc_vector[ig]==ret_uname_bank_2[48] || s_pc_vector[ig]==ret_uname_bank_2[49] || 
			    s_pc_vector[ig]==ret_uname_bank_2[50] || s_pc_vector[ig]==ret_uname_bank_2[51] ||
			    s_pc_vector[ig]==ret_uname_bank_2[52] || s_pc_vector[ig]==ret_uname_bank_2[53] || 
			    s_pc_vector[ig]==ret_uname_bank_2[54] || s_pc_vector[ig]==ret_uname_bank_2[55] ||
			    s_pc_vector[ig]==ret_uname_bank_2[56] || s_pc_vector[ig]==ret_uname_bank_2[57] || 
			    s_pc_vector[ig]==ret_uname_bank_2[58] || s_pc_vector[ig]==ret_uname_bank_2[59] ||
			    s_pc_vector[ig]==ret_uname_bank_2[60] || s_pc_vector[ig]==ret_uname_bank_2[61] || 
			    s_pc_vector[ig]==ret_uname_bank_2[62] || s_pc_vector[ig]==ret_uname_bank_2[63] ||
			    s_pc_vector[ig]==ret_uname_bank_3[0] || s_pc_vector[ig]==ret_uname_bank_3[1] || 
			    s_pc_vector[ig]==ret_uname_bank_3[2] || s_pc_vector[ig]==ret_uname_bank_3[3] ||
			    s_pc_vector[ig]==ret_uname_bank_3[4] || s_pc_vector[ig]==ret_uname_bank_3[5] || 
			    s_pc_vector[ig]==ret_uname_bank_3[6] || s_pc_vector[ig]==ret_uname_bank_3[7] ||
			    s_pc_vector[ig]==ret_uname_bank_3[8] || s_pc_vector[ig]==ret_uname_bank_3[9] || 
			    s_pc_vector[ig]==ret_uname_bank_3[10] || s_pc_vector[ig]==ret_uname_bank_3[11] ||
			    s_pc_vector[ig]==ret_uname_bank_3[12] || s_pc_vector[ig]==ret_uname_bank_3[13] || 
			    s_pc_vector[ig]==ret_uname_bank_3[14] || s_pc_vector[ig]==ret_uname_bank_3[15] ||
			    s_pc_vector[ig]==ret_uname_bank_3[16] || s_pc_vector[ig]==ret_uname_bank_3[17] || 
			    s_pc_vector[ig]==ret_uname_bank_3[18] || s_pc_vector[ig]==ret_uname_bank_3[19] ||
			    s_pc_vector[ig]==ret_uname_bank_3[20] || s_pc_vector[ig]==ret_uname_bank_3[21] || 
			    s_pc_vector[ig]==ret_uname_bank_3[22] || s_pc_vector[ig]==ret_uname_bank_3[23] ||
			    s_pc_vector[ig]==ret_uname_bank_3[24] || s_pc_vector[ig]==ret_uname_bank_3[25] || 
			    s_pc_vector[ig]==ret_uname_bank_3[26] || s_pc_vector[ig]==ret_uname_bank_3[27] ||
			    s_pc_vector[ig]==ret_uname_bank_3[28] || s_pc_vector[ig]==ret_uname_bank_3[29] || 
			    s_pc_vector[ig]==ret_uname_bank_3[30] || s_pc_vector[ig]==ret_uname_bank_3[31] ||
			    s_pc_vector[ig]==ret_uname_bank_3[32] || s_pc_vector[ig]==ret_uname_bank_3[33] || 
			    s_pc_vector[ig]==ret_uname_bank_3[34] || s_pc_vector[ig]==ret_uname_bank_3[35] ||
			    s_pc_vector[ig]==ret_uname_bank_3[36] || s_pc_vector[ig]==ret_uname_bank_3[37] || 
			    s_pc_vector[ig]==ret_uname_bank_3[38] || s_pc_vector[ig]==ret_uname_bank_3[39] ||
			    s_pc_vector[ig]==ret_uname_bank_3[40] || s_pc_vector[ig]==ret_uname_bank_3[41] || 
			    s_pc_vector[ig]==ret_uname_bank_3[42] || s_pc_vector[ig]==ret_uname_bank_3[43] ||
			    s_pc_vector[ig]==ret_uname_bank_3[44] || s_pc_vector[ig]==ret_uname_bank_3[45] || 
			    s_pc_vector[ig]==ret_uname_bank_3[46] || s_pc_vector[ig]==ret_uname_bank_3[47] ||
			    s_pc_vector[ig]==ret_uname_bank_3[48] || s_pc_vector[ig]==ret_uname_bank_3[49] || 
			    s_pc_vector[ig]==ret_uname_bank_3[50] || s_pc_vector[ig]==ret_uname_bank_3[51] ||
			    s_pc_vector[ig]==ret_uname_bank_3[52] || s_pc_vector[ig]==ret_uname_bank_3[53] || 
			    s_pc_vector[ig]==ret_uname_bank_3[54] || s_pc_vector[ig]==ret_uname_bank_3[55] ||
			    s_pc_vector[ig]==ret_uname_bank_3[56] || s_pc_vector[ig]==ret_uname_bank_3[57] || 
			    s_pc_vector[ig]==ret_uname_bank_3[58] || s_pc_vector[ig]==ret_uname_bank_3[59] ||
			    s_pc_vector[ig]==ret_uname_bank_3[60] || s_pc_vector[ig]==ret_uname_bank_3[61] || 
			    s_pc_vector[ig]==ret_uname_bank_3[62] || s_pc_vector[ig]==ret_uname_bank_3[63])
			 */
			/* simplier version : ! works only if threads are binded on proc */ 
			if (s_pc_vector[ig]==mutex_lock_addr || s_pc_vector[ig]==mutex_unlock_addr || 
			    s_pc_vector[ig]==lock_wait_addr || s_pc_vector[ig]==lock_wait_priv_addr || 
			    s_pc_vector[ig]==barrier_addr || s_pc_vector[ig]==barrier_wait_end_addr || 
			    s_pc_vector[ig]==barrier_instr_futex_wake_addr || 
			    s_pc_vector[ig]==main_start_addr || s_pc_vector[ig]== return_main_addr ||
			    s_pc_vector[ig]==ll_instr_addr || s_pc_vector[ig]==ll_instr_addr_2 ||
			    s_pc_vector[ig]==futex_wake_addr || s_pc_vector[ig]==fct_uname_1_addr ||
			    s_pc_vector[ig]==fct_uname_2_addr || s_pc_vector[ig]==fct_uname_3_addr ||
			    s_pc_vector[ig]==start_interest_section_addr || s_pc_vector[ig]==end_interest_section_addr ||
			    s_pc_vector[ig]==r31_bank[ig] || s_pc_vector[ig]==ret_futex_wake_bank[ig] || 
			    s_pc_vector[ig]==ret_uname_bank_1[ig] || s_pc_vector[ig]==ret_uname_bank_2[ig] ||
			    s_pc_vector[ig]==ret_uname_bank_3[ig])
			  begin
			     dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
					  s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
			     // we re-intialize the bank entry only if the lock enter and return
			     // append on the same proc (process affinity)
			     // else we don't re-initialize because we can't manage multiple
			     // simultaneous enter and since value a filter again in HVL
			     // it is preferable to jump to HVL more often than requiered
			     // to avoid data missing.
			     if (s_pc_vector[ig] == r31_bank[ig])
			       begin
				  r31_bank[ig] <= 0;
			       end

			     // end of interest function
			     if (s_pc_vector[ig]==ret_uname_bank_3[ig])
			       begin
				  interest_function[ig] = 0;
			       end
			  end // if (s_pc_vector[ig]==mutex_lock_addr || s_pc_vector[ig]==mutex_unlock_addr ||...
			if (s_pc_vector[ig] == mutex_lock_addr)
			  begin
			     r31_bank[ig] <= s_r31_vector[ig]; 
			  end
			if (s_pc_vector[ig] == futex_wake_addr)
			  begin
			     ret_futex_wake_bank[ig] <= s_r31_vector[ig]; 
			  end
			if (s_pc_vector[ig] == fct_uname_1_addr)
			  begin
			     ret_uname_bank_1[ig] <= s_r31_vector[ig]; 
			  end
			if (s_pc_vector[ig] == fct_uname_2_addr)
			  begin
			     ret_uname_bank_2[ig] <= s_r31_vector[ig]; 
			  end
			if (s_pc_vector[ig] == fct_uname_3_addr)
			  begin
			     interest_function[ig] = 1;
			     interest_section_core = ig;
			     ret_uname_bank_3[ig] <= s_r31_vector[ig]; 
			  end
			if (s_pc_vector[ig] == start_interest_section_addr)
			  begin
			     global_interest_section = 1;
			     interest_section_core = ig;
			     interest_section[ig] = 1;
			  end
			if (s_pc_vector[ig] == end_interest_section_addr)
			  begin
			     global_interest_section = 0;
			     interest_section[ig] = 0;
			  end
			if (interest_function[ig] == 1)
			  begin
			     //dump_signals(s_pc_vector[ig], s_r6_vector[ig], s_r28_vector[ig], s_r29_vector[ig], s_r31_vector[ig], clock, ig);
			     //dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
			     //if (interest_section_core == ig)
			     if ((detailled_interest_section >= 10) && (detailled_interest_section < 20))
			       begin
				  dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
					       s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
			       end
			     // jal or jalr instruction detected
			     // jr instruction detected
			     if ((detailled_interest_section < 10) && 
				 (((s_instr_vector[ig]&32'hFC000000) == 32'h0C000000) || ((s_instr_vector[ig]&32'hFC1F07FF) == 32'h00000009) ||
				  ((s_instr_vector[ig]&32'hFC1FFFFF) == 32'h00000008)))
			       begin
				  dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
					       s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
			       end

			     if ((detailled_interest_section == 20) && (prev_L1_phy_addr[ig] != L1_s_phy_addr[ig]) && (L1_s_phy_addr[ig] != 0) && (L1_cmdval[ig] == 1)) 
			       begin
				  dump_L1_vci(s_pc_vector[ig], s_instr_vector[ig], L1_vci_cmd[ig], L1_s_phy_addr[ig], L1_cmd_srcid[ig], L1_cmd_trdid[ig], L1_cmd_pktid[ig], clock, ig);
				  prev_L1_phy_addr[ig] <= L1_s_phy_addr[ig];
				  triggered_srcid[ig] <= L1_cmd_srcid[ig];
				  triggered_trdid[ig] <= L1_cmd_trdid[ig];
				  triggered_pktid[ig] <= L1_cmd_pktid[ig];
			       end

			     if ((detailled_interest_section == 30) && (next_instr_trig[ig] == 1) && (s_pc_vector[ig] != prev_pc_bank[ig]))
			       begin
				  dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
					       s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
				  next_instr_trig[ig] = 0;
				  prev_pc_bank[ig] <= 0;
			       end
			     if ((detailled_interest_section == 30) && (((s_instr_vector[ig]&mask_trig_instr_1)==trig_instr_1) || ((s_instr_vector[ig]&mask_trig_instr_2)==trig_instr_2)))
			       begin
			          if (s_pc_vector[ig] != prev_pc_bank[ig])
				    begin
				       dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
						    s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
				    end
				  next_instr_trig[ig] = 1;
				  prev_pc_bank[ig] <= s_pc_vector[ig];
				  ll_sc_pending[ig] = 1;
			       end
			  end // if (interest_function[ig] == 1)
			if ((detailled_interest_section == 30) && (ll_sc_pending[ig] == 1) && (L1_s_phy_addr[ig] != 0) && (L1_vci_cmd[ig] == 2'b11) && (L1_cmdval[ig] == 1) &&
			    (((prev_pc_bank[ig] == s_pc_vector[ig]) && (prev_L1_phy_addr[ig] != L1_s_phy_addr[ig])) || (prev_pc_bank[ig] != s_pc_vector[ig]))) // ll cmd
			  begin
			     dump_L1_vci(s_pc_vector[ig], s_instr_vector[ig], L1_vci_cmd[ig], L1_s_phy_addr[ig], L1_cmd_srcid[ig], L1_cmd_trdid[ig], L1_cmd_pktid[ig], clock, ig);
			     ll_sc_pending[ig] = 0;
			     prev_L1_phy_addr[ig] <= L1_s_phy_addr[ig];
			     prev_pc_bank[ig] <= s_pc_vector[ig];
			     triggered_srcid[ig] <= L1_cmd_srcid[ig];
			     triggered_trdid[ig] <= L1_cmd_trdid[ig];
			     triggered_pktid[ig] <= L1_cmd_pktid[ig];
			  end
			if ((global_interest_section == 1) && (detailled_interest_section < 10))
			  begin
			     // jal or jalr instruction detected
			     // jr instruction detected
			     if (((s_instr_vector[ig]&32'hFC000000) == 32'h0C000000) || ((s_instr_vector[ig]&32'hFC1F07FF) == 32'h00000009) ||
				 ((s_instr_vector[ig]&32'hFC1FFFFF) == 32'h00000008))
			       begin
				  dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
					       s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
			       end
			  end // if (global_interest_section == 1)
			if ((detailled_interest_section != 30) && (next_instr_trig[ig] == 1) && (s_pc_vector[ig] != prev_pc_bank[ig]))
			  begin
			     dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
					  s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
			     next_instr_trig[ig] = 0;
			     prev_pc_bank[ig] <= 0;
			  end
			if ((interest_section[ig] == 1) && (detailled_interest_section >= 10) && (detailled_interest_section < 30)&& 
			    (((s_instr_vector[ig]&mask_trig_instr_1)==trig_instr_1) || ((s_instr_vector[ig]&mask_trig_instr_2)==trig_instr_2)))
			  begin
			     if (s_pc_vector[ig] != prev_pc_bank[ig])
			       begin
				  dump_signals(s_pc_vector[ig], s_instr_vector[ig], s_r6_vector[ig], s_r28_vector[ig], 
					       s_r29_vector[ig], s_r31_vector[ig], clock, clock_bis, ig);
			       end
			     next_instr_trig[ig] = 1;
			     prev_pc_bank[ig] <= s_pc_vector[ig];
			  end // if ((interest_section[ig] == 1) && (detailled_interest_section >= 10) &&...
		     end // if (in_main_prog == 1)

		   if ((s_pc_vector[ig]==return_main_addr) && (clock > 100))
		     begin
			dump_signals(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, main_start_addr, ll_instr_addr, 2000, 2000, 101);
			load_trig_addresses(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, barrier_addr, barrier_wait_end_addr, 
					    barrier_instr_futex_wake_addr, main_start_addr, ll_instr_addr, ll_instr_addr_2, futex_wake_addr, 
					    fct_uname_1_addr, fct_uname_2_addr, fct_uname_3_addr, start_interest_section_addr, end_interest_section_addr, 
					    return_main_addr, trig_instr_1, mask_trig_instr_1, trig_instr_2, mask_trig_instr_2, detailled_interest_section);
			dump_signals(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, main_start_addr, ll_instr_addr, 2000, 2000, 100);
			in_main_prog = 0;
			global_interest_section = 0;
			for (i=0; i<64; i++) begin
			   r31_bank[i] <= 0;
			   ret_futex_wake_bank[i] <= 0;
			   ret_uname_bank_1[i] <= 0;
			   ret_uname_bank_2[i] <= 0;
			   ret_uname_bank_3[i] <= 0;
			   interest_section[i] = 0;
			   triggered_srcid[i] <= 0;
			   triggered_trdid[i] <= 0;
			   triggered_pktid[i] <= 0;
			   prev_L1_phy_addr[i] <= 0;
			end // for (i=0; i<64; i++)

			for (i=0; i<16; i++) begin
			   prev_L2_phy_addr[i] <= 0;
			   prev_L2_cmd_srcid[i] <= 0;
			   prev_L2_cmd_trdid[i] <= 0;
			   prev_L2_cmd_pktid[i] <= 0;
			   L2_cmd_in_time[i] <= 0;
			end
			
			clock <= 0; // re-initialize the clock for the new program dump
			clock_bis <= 0;
		     end
		   
		end // if (s_pc_vector[ig] != 0)
	   end // always @ (posedge clk)
      end // block: always
   endgenerate

   generate
      genvar igc;
      for (igc=0; igc < 16; igc++)  begin: module_gen_cluster
	 always @(posedge clk) 
	   begin
	      if ((in_main_prog == 1) && (detailled_interest_section == 15) && (L2_cmdval[igc] == 1))
		begin
		   L2_cmd_in_time[igc] <= clock;
		end

	      if ((in_main_prog == 1) && (detailled_interest_section == 15) && (L2_cmdack[igc] == 1) && (L2_cmd_in_time[igc] != 0))
		begin
		   dump_L2_workload(L2_cmd_in_time[igc], clock, igc);
		   L2_cmd_in_time[igc] <= 0; //reinitialization of the variable once the pending cmd is procedded
		end

	      if ((in_main_prog == 1) && (detailled_interest_section == 16) && (xbar_r_allocated[igc] == 1) && (xbar_rel[igc] == 0) && (xbar_req[igc] > 0))
		begin
		   dump_xbar_workload(xbar_req[igc], clock, igc);
		end
		  
	      if ((in_main_prog == 1) && (detailled_interest_section >= 20) && 
		  ((prev_L2_phy_addr[igc] != L2_s_phy_addr[igc]) || (prev_L2_cmd_srcid[igc] != L2_cmd_srcid[igc]) || (prev_L2_cmd_trdid[igc] != L2_cmd_trdid[igc]) || (prev_L2_cmd_pktid[igc] != L2_cmd_pktid[igc])) && (L2_s_phy_addr[igc] != 0) && (L2_cmdval[igc] == 1))
		begin
		   if ((triggered_srcid[0] == L2_cmd_srcid[igc]) && (triggered_trdid[0] == L2_cmd_trdid[igc]) && (triggered_pktid[0] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[0] <= 0;
			triggered_trdid[0] <= 0;
			triggered_pktid[0] <= 0;
		     end 

		   if ((triggered_srcid[1] == L2_cmd_srcid[igc]) && (triggered_trdid[1] == L2_cmd_trdid[igc]) && (triggered_pktid[1] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[1] <= 0;
			triggered_trdid[1] <= 0;
			triggered_pktid[1] <= 0;
		     end

		   if ((triggered_srcid[2] == L2_cmd_srcid[igc]) && (triggered_trdid[2] == L2_cmd_trdid[igc]) && (triggered_pktid[2] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[2] <= 0;
			triggered_trdid[2] <= 0;
			triggered_pktid[2] <= 0;
		     end 

		   if ((triggered_srcid[3] == L2_cmd_srcid[igc]) && (triggered_trdid[3] == L2_cmd_trdid[igc]) && (triggered_pktid[3] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[3] <= 0;
			triggered_trdid[3] <= 0;
			triggered_pktid[3] <= 0;
		     end

		   if ((triggered_srcid[4] == L2_cmd_srcid[igc]) && (triggered_trdid[4] == L2_cmd_trdid[igc]) && (triggered_pktid[4] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[4] <= 0;
			triggered_trdid[4] <= 0;
			triggered_pktid[4] <= 0;
		     end 

		   if ((triggered_srcid[5] == L2_cmd_srcid[igc]) && (triggered_trdid[5] == L2_cmd_trdid[igc]) && (triggered_pktid[5] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[5] <= 0;
			triggered_trdid[5] <= 0;
			triggered_pktid[5] <= 0;
		     end

		   if ((triggered_srcid[6] == L2_cmd_srcid[igc]) && (triggered_trdid[6] == L2_cmd_trdid[igc]) && (triggered_pktid[6] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[6] <= 0;
			triggered_trdid[6] <= 0;
			triggered_pktid[6] <= 0;
		     end 

		   if ((triggered_srcid[7] == L2_cmd_srcid[igc]) && (triggered_trdid[7] == L2_cmd_trdid[igc]) && (triggered_pktid[7] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[7] <= 0;
			triggered_trdid[7] <= 0;
			triggered_pktid[7] <= 0;
		     end

		   if ((triggered_srcid[8] == L2_cmd_srcid[igc]) && (triggered_trdid[8] == L2_cmd_trdid[igc]) && (triggered_pktid[8] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[8] <= 0;
			triggered_trdid[8] <= 0;
			triggered_pktid[8] <= 0;
		     end 

		   if ((triggered_srcid[9] == L2_cmd_srcid[igc]) && (triggered_trdid[9] == L2_cmd_trdid[igc]) && (triggered_pktid[9] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[9] <= 0;
			triggered_trdid[9] <= 0;
			triggered_pktid[9] <= 0;
		     end

		   if ((triggered_srcid[10] == L2_cmd_srcid[igc]) && (triggered_trdid[10] == L2_cmd_trdid[igc]) && (triggered_pktid[10] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[10] <= 0;
			triggered_trdid[10] <= 0;
			triggered_pktid[10] <= 0;
		     end 

		   if ((triggered_srcid[11] == L2_cmd_srcid[igc]) && (triggered_trdid[11] == L2_cmd_trdid[igc]) && (triggered_pktid[11] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[11] <= 0;
			triggered_trdid[11] <= 0;
			triggered_pktid[11] <= 0;
		     end

		   if ((triggered_srcid[12] == L2_cmd_srcid[igc]) && (triggered_trdid[12] == L2_cmd_trdid[igc]) && (triggered_pktid[12] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[12] <= 0;
			triggered_trdid[12] <= 0;
			triggered_pktid[12] <= 0;
		     end 

		   if ((triggered_srcid[13] == L2_cmd_srcid[igc]) && (triggered_trdid[13] == L2_cmd_trdid[igc]) && (triggered_pktid[13] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[13] <= 0;
			triggered_trdid[13] <= 0;
			triggered_pktid[13] <= 0;
		     end

		   if ((triggered_srcid[14] == L2_cmd_srcid[igc]) && (triggered_trdid[14] == L2_cmd_trdid[igc]) && (triggered_pktid[14] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[14] <= 0;
			triggered_trdid[14] <= 0;
			triggered_pktid[14] <= 0;
		     end 

		   if ((triggered_srcid[15] == L2_cmd_srcid[igc]) && (triggered_trdid[15] == L2_cmd_trdid[igc]) && (triggered_pktid[15] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[15] <= 0;
			triggered_trdid[15] <= 0;
			triggered_pktid[15] <= 0;
		     end

		   if ((triggered_srcid[16] == L2_cmd_srcid[igc]) && (triggered_trdid[16] == L2_cmd_trdid[igc]) && (triggered_pktid[16] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[16] <= 0;
			triggered_trdid[16] <= 0;
			triggered_pktid[16] <= 0;
		     end 

		   if ((triggered_srcid[17] == L2_cmd_srcid[igc]) && (triggered_trdid[17] == L2_cmd_trdid[igc]) && (triggered_pktid[17] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[17] <= 0;
			triggered_trdid[17] <= 0;
			triggered_pktid[17] <= 0;
		     end // if ((triggered_srcid[17] == L2_cmd_srcid[igc]) && (triggered_trdid[17] == L2_cmd_trdid[igc]) && (triggered_pktid[17] == L2_cmd_pktid[igc]))

		   if ((triggered_srcid[18] == L2_cmd_srcid[igc]) && (triggered_trdid[18] == L2_cmd_trdid[igc]) && (triggered_pktid[18] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[18] <= 0;
			triggered_trdid[18] <= 0;
			triggered_pktid[18] <= 0;
		     end

		   if ((triggered_srcid[19] == L2_cmd_srcid[igc]) && (triggered_trdid[19] == L2_cmd_trdid[igc]) && (triggered_pktid[19] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[19] <= 0;
			triggered_trdid[19] <= 0;
			triggered_pktid[19] <= 0;
		     end 

		   if ((triggered_srcid[20] == L2_cmd_srcid[igc]) && (triggered_trdid[20] == L2_cmd_trdid[igc]) && (triggered_pktid[20] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[20] <= 0;
			triggered_trdid[20] <= 0;
			triggered_pktid[20] <= 0;
		     end

		   if ((triggered_srcid[21] == L2_cmd_srcid[igc]) && (triggered_trdid[21] == L2_cmd_trdid[igc]) && (triggered_pktid[21] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[21] <= 0;
			triggered_trdid[21] <= 0;
			triggered_pktid[21] <= 0;
		     end 

		   if ((triggered_srcid[22] == L2_cmd_srcid[igc]) && (triggered_trdid[22] == L2_cmd_trdid[igc]) && (triggered_pktid[22] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[22] <= 0;
			triggered_trdid[22] <= 0;
			triggered_pktid[22] <= 0;
		     end

		   if ((triggered_srcid[23] == L2_cmd_srcid[igc]) && (triggered_trdid[23] == L2_cmd_trdid[igc]) && (triggered_pktid[23] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[23] <= 0;
			triggered_trdid[23] <= 0;
			triggered_pktid[23] <= 0;
		     end 

		   if ((triggered_srcid[24] == L2_cmd_srcid[igc]) && (triggered_trdid[24] == L2_cmd_trdid[igc]) && (triggered_pktid[24] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[24] <= 0;
			triggered_trdid[24] <= 0;
			triggered_pktid[24] <= 0;
		     end

		   if ((triggered_srcid[25] == L2_cmd_srcid[igc]) && (triggered_trdid[25] == L2_cmd_trdid[igc]) && (triggered_pktid[25] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[25] <= 0;
			triggered_trdid[25] <= 0;
			triggered_pktid[25] <= 0;
		     end 

		   if ((triggered_srcid[26] == L2_cmd_srcid[igc]) && (triggered_trdid[26] == L2_cmd_trdid[igc]) && (triggered_pktid[26] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[26] <= 0;
			triggered_trdid[26] <= 0;
			triggered_pktid[26] <= 0;
		     end

		   if ((triggered_srcid[27] == L2_cmd_srcid[igc]) && (triggered_trdid[27] == L2_cmd_trdid[igc]) && (triggered_pktid[27] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[27] <= 0;
			triggered_trdid[27] <= 0;
			triggered_pktid[27] <= 0;
		     end 

		   if ((triggered_srcid[28] == L2_cmd_srcid[igc]) && (triggered_trdid[28] == L2_cmd_trdid[igc]) && (triggered_pktid[28] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[28] <= 0;
			triggered_trdid[28] <= 0;
			triggered_pktid[28] <= 0;
		     end

		   if ((triggered_srcid[29] == L2_cmd_srcid[igc]) && (triggered_trdid[29] == L2_cmd_trdid[igc]) && (triggered_pktid[29] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[29] <= 0;
			triggered_trdid[29] <= 0;
			triggered_pktid[29] <= 0;
		     end 

		   if ((triggered_srcid[30] == L2_cmd_srcid[igc]) && (triggered_trdid[30] == L2_cmd_trdid[igc]) && (triggered_pktid[30] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[30] <= 0;
			triggered_trdid[30] <= 0;
			triggered_pktid[30] <= 0;
		     end

		   if ((triggered_srcid[31] == L2_cmd_srcid[igc]) && (triggered_trdid[31] == L2_cmd_trdid[igc]) && (triggered_pktid[31] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[31] <= 0;
			triggered_trdid[31] <= 0;
			triggered_pktid[31] <= 0;
		     end 

		   if ((triggered_srcid[32] == L2_cmd_srcid[igc]) && (triggered_trdid[32] == L2_cmd_trdid[igc]) && (triggered_pktid[32] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[32] <= 0;
			triggered_trdid[32] <= 0;
			triggered_pktid[32] <= 0;
		     end

		   if ((triggered_srcid[33] == L2_cmd_srcid[igc]) && (triggered_trdid[33] == L2_cmd_trdid[igc]) && (triggered_pktid[33] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[33] <= 0;
			triggered_trdid[33] <= 0;
			triggered_pktid[33] <= 0;
		     end 

		   if ((triggered_srcid[34] == L2_cmd_srcid[igc]) && (triggered_trdid[34] == L2_cmd_trdid[igc]) && (triggered_pktid[34] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[34] <= 0;
			triggered_trdid[34] <= 0;
			triggered_pktid[34] <= 0;
		     end

		   if ((triggered_srcid[35] == L2_cmd_srcid[igc]) && (triggered_trdid[35] == L2_cmd_trdid[igc]) && (triggered_pktid[35] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[35] <= 0;
			triggered_trdid[35] <= 0;
			triggered_pktid[35] <= 0;
		     end 

		   if ((triggered_srcid[36] == L2_cmd_srcid[igc]) && (triggered_trdid[36] == L2_cmd_trdid[igc]) && (triggered_pktid[36] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[36] <= 0;
			triggered_trdid[36] <= 0;
			triggered_pktid[36] <= 0;
		     end

		   if ((triggered_srcid[37] == L2_cmd_srcid[igc]) && (triggered_trdid[37] == L2_cmd_trdid[igc]) && (triggered_pktid[37] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[37] <= 0;
			triggered_trdid[37] <= 0;
			triggered_pktid[37] <= 0;
		     end 

		   if ((triggered_srcid[38] == L2_cmd_srcid[igc]) && (triggered_trdid[38] == L2_cmd_trdid[igc]) && (triggered_pktid[38] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[38] <= 0;
			triggered_trdid[38] <= 0;
			triggered_pktid[38] <= 0;
		     end

		   if ((triggered_srcid[39] == L2_cmd_srcid[igc]) && (triggered_trdid[39] == L2_cmd_trdid[igc]) && (triggered_pktid[39] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[39] <= 0;
			triggered_trdid[39] <= 0;
			triggered_pktid[39] <= 0;
		     end 

		   if ((triggered_srcid[40] == L2_cmd_srcid[igc]) && (triggered_trdid[40] == L2_cmd_trdid[igc]) && (triggered_pktid[40] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[40] <= 0;
			triggered_trdid[40] <= 0;
			triggered_pktid[40] <= 0;
		     end

		   if ((triggered_srcid[41] == L2_cmd_srcid[igc]) && (triggered_trdid[41] == L2_cmd_trdid[igc]) && (triggered_pktid[41] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[41] <= 0;
			triggered_trdid[41] <= 0;
			triggered_pktid[41] <= 0;
		     end 

		   if ((triggered_srcid[42] == L2_cmd_srcid[igc]) && (triggered_trdid[42] == L2_cmd_trdid[igc]) && (triggered_pktid[42] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[42] <= 0;
			triggered_trdid[42] <= 0;
			triggered_pktid[42] <= 0;
		     end

		   if ((triggered_srcid[43] == L2_cmd_srcid[igc]) && (triggered_trdid[43] == L2_cmd_trdid[igc]) && (triggered_pktid[43] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[43] <= 0;
			triggered_trdid[43] <= 0;
			triggered_pktid[43] <= 0;
		     end 

		   if ((triggered_srcid[44] == L2_cmd_srcid[igc]) && (triggered_trdid[44] == L2_cmd_trdid[igc]) && (triggered_pktid[44] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[44] <= 0;
			triggered_trdid[44] <= 0;
			triggered_pktid[44] <= 0;
		     end

		   if ((triggered_srcid[45] == L2_cmd_srcid[igc]) && (triggered_trdid[45] == L2_cmd_trdid[igc]) && (triggered_pktid[45] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[45] <= 0;
			triggered_trdid[45] <= 0;
			triggered_pktid[45] <= 0;
		     end 

		   if ((triggered_srcid[46] == L2_cmd_srcid[igc]) && (triggered_trdid[46] == L2_cmd_trdid[igc]) && (triggered_pktid[46] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[46] <= 0;
			triggered_trdid[46] <= 0;
			triggered_pktid[46] <= 0;
		     end

		   if ((triggered_srcid[47] == L2_cmd_srcid[igc]) && (triggered_trdid[47] == L2_cmd_trdid[igc]) && (triggered_pktid[47] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[47] <= 0;
			triggered_trdid[47] <= 0;
			triggered_pktid[47] <= 0;
		     end 

		   if ((triggered_srcid[48] == L2_cmd_srcid[igc]) && (triggered_trdid[48] == L2_cmd_trdid[igc]) && (triggered_pktid[48] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[48] <= 0;
			triggered_trdid[48] <= 0;
			triggered_pktid[48] <= 0;
		     end

		   if ((triggered_srcid[49] == L2_cmd_srcid[igc]) && (triggered_trdid[49] == L2_cmd_trdid[igc]) && (triggered_pktid[49] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[49] <= 0;
			triggered_trdid[49] <= 0;
			triggered_pktid[49] <= 0;
		     end 

		   if ((triggered_srcid[50] == L2_cmd_srcid[igc]) && (triggered_trdid[50] == L2_cmd_trdid[igc]) && (triggered_pktid[50] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[50] <= 0;
			triggered_trdid[50] <= 0;
			triggered_pktid[50] <= 0;
		     end

		   if ((triggered_srcid[51] == L2_cmd_srcid[igc]) && (triggered_trdid[51] == L2_cmd_trdid[igc]) && (triggered_pktid[51] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[51] <= 0;
			triggered_trdid[51] <= 0;
			triggered_pktid[51] <= 0;
		     end 

		   if ((triggered_srcid[52] == L2_cmd_srcid[igc]) && (triggered_trdid[52] == L2_cmd_trdid[igc]) && (triggered_pktid[52] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[52] <= 0;
			triggered_trdid[52] <= 0;
			triggered_pktid[52] <= 0;
		     end

		   if ((triggered_srcid[53] == L2_cmd_srcid[igc]) && (triggered_trdid[53] == L2_cmd_trdid[igc]) && (triggered_pktid[53] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[53] <= 0;
			triggered_trdid[53] <= 0;
			triggered_pktid[53] <= 0;
		     end 

		   if ((triggered_srcid[54] == L2_cmd_srcid[igc]) && (triggered_trdid[54] == L2_cmd_trdid[igc]) && (triggered_pktid[54] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[54] <= 0;
			triggered_trdid[54] <= 0;
			triggered_pktid[54] <= 0;
		     end

		   if ((triggered_srcid[55] == L2_cmd_srcid[igc]) && (triggered_trdid[55] == L2_cmd_trdid[igc]) && (triggered_pktid[55] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[55] <= 0;
			triggered_trdid[55] <= 0;
			triggered_pktid[55] <= 0;
		     end 

		   if ((triggered_srcid[56] == L2_cmd_srcid[igc]) && (triggered_trdid[56] == L2_cmd_trdid[igc]) && (triggered_pktid[56] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[56] <= 0;
			triggered_trdid[56] <= 0;
			triggered_pktid[56] <= 0;
		     end

		   if ((triggered_srcid[57] == L2_cmd_srcid[igc]) && (triggered_trdid[57] == L2_cmd_trdid[igc]) && (triggered_pktid[57] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[57] <= 0;
			triggered_trdid[57] <= 0;
			triggered_pktid[57] <= 0;
		     end 

		   if ((triggered_srcid[58] == L2_cmd_srcid[igc]) && (triggered_trdid[58] == L2_cmd_trdid[igc]) && (triggered_pktid[58] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[58] <= 0;
			triggered_trdid[58] <= 0;
			triggered_pktid[58] <= 0;
		     end

		   if ((triggered_srcid[59] == L2_cmd_srcid[igc]) && (triggered_trdid[59] == L2_cmd_trdid[igc]) && (triggered_pktid[59] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[59] <= 0;
			triggered_trdid[59] <= 0;
			triggered_pktid[59] <= 0;
		     end 

		   if ((triggered_srcid[60] == L2_cmd_srcid[igc]) && (triggered_trdid[60] == L2_cmd_trdid[igc]) && (triggered_pktid[60] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[60] <= 0;
			triggered_trdid[60] <= 0;
			triggered_pktid[60] <= 0;
		     end

		   if ((triggered_srcid[61] == L2_cmd_srcid[igc]) && (triggered_trdid[61] == L2_cmd_trdid[igc]) && (triggered_pktid[61] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[61] <= 0;
			triggered_trdid[61] <= 0;
			triggered_pktid[61] <= 0;
		     end 

		   if ((triggered_srcid[62] == L2_cmd_srcid[igc]) && (triggered_trdid[62] == L2_cmd_trdid[igc]) && (triggered_pktid[62] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[62] <= 0;
			triggered_trdid[62] <= 0;
			triggered_pktid[62] <= 0;
		     end

		   if ((triggered_srcid[63] == L2_cmd_srcid[igc]) && (triggered_trdid[63] == L2_cmd_trdid[igc]) && (triggered_pktid[63] == L2_cmd_pktid[igc]))
		     begin
			dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
			prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
			prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
			prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
			prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
			triggered_srcid[63] <= 0;
			triggered_trdid[63] <= 0;
			triggered_pktid[63] <= 0;
		     end 
/*
		    ((triggered_srcid[1] == L2_cmd_srcid[igc]) && (triggered_trdid[1] == L2_cmd_trdid[igc]) && (triggered_pktid[1] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[2] == L2_cmd_srcid[igc]) && (triggered_trdid[2] == L2_cmd_trdid[igc]) && (triggered_pktid[2] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[3] == L2_cmd_srcid[igc]) && (triggered_trdid[3] == L2_cmd_trdid[igc]) && (triggered_pktid[3] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[4] == L2_cmd_srcid[igc]) && (triggered_trdid[4] == L2_cmd_trdid[igc]) && (triggered_pktid[4] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[5] == L2_cmd_srcid[igc]) && (triggered_trdid[5] == L2_cmd_trdid[igc]) && (triggered_pktid[5] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[6] == L2_cmd_srcid[igc]) && (triggered_trdid[6] == L2_cmd_trdid[igc]) && (triggered_pktid[6] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[7] == L2_cmd_srcid[igc]) && (triggered_trdid[7] == L2_cmd_trdid[igc]) && (triggered_pktid[7] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[8] == L2_cmd_srcid[igc]) && (triggered_trdid[8] == L2_cmd_trdid[igc]) && (triggered_pktid[8] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[9] == L2_cmd_srcid[igc]) && (triggered_trdid[9] == L2_cmd_trdid[igc]) && (triggered_pktid[9] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[10] == L2_cmd_srcid[igc]) && (triggered_trdid[10] == L2_cmd_trdid[igc]) && (triggered_pktid[10] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[11] == L2_cmd_srcid[igc]) && (triggered_trdid[11] == L2_cmd_trdid[igc]) && (triggered_pktid[11] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[12] == L2_cmd_srcid[igc]) && (triggered_trdid[12] == L2_cmd_trdid[igc]) && (triggered_pktid[12] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[13] == L2_cmd_srcid[igc]) && (triggered_trdid[13] == L2_cmd_trdid[igc]) && (triggered_pktid[13] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[14] == L2_cmd_srcid[igc]) && (triggered_trdid[14] == L2_cmd_trdid[igc]) && (triggered_pktid[14] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[15] == L2_cmd_srcid[igc]) && (triggered_trdid[15] == L2_cmd_trdid[igc]) && (triggered_pktid[15] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[16] == L2_cmd_srcid[igc]) && (triggered_trdid[16] == L2_cmd_trdid[igc]) && (triggered_pktid[16] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[17] == L2_cmd_srcid[igc]) && (triggered_trdid[17] == L2_cmd_trdid[igc]) && (triggered_pktid[17] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[18] == L2_cmd_srcid[igc]) && (triggered_trdid[18] == L2_cmd_trdid[igc]) && (triggered_pktid[18] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[19] == L2_cmd_srcid[igc]) && (triggered_trdid[19] == L2_cmd_trdid[igc]) && (triggered_pktid[19] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[20] == L2_cmd_srcid[igc]) && (triggered_trdid[20] == L2_cmd_trdid[igc]) && (triggered_pktid[20] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[21] == L2_cmd_srcid[igc]) && (triggered_trdid[21] == L2_cmd_trdid[igc]) && (triggered_pktid[21] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[22] == L2_cmd_srcid[igc]) && (triggered_trdid[22] == L2_cmd_trdid[igc]) && (triggered_pktid[22] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[23] == L2_cmd_srcid[igc]) && (triggered_trdid[23] == L2_cmd_trdid[igc]) && (triggered_pktid[23] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[24] == L2_cmd_srcid[igc]) && (triggered_trdid[24] == L2_cmd_trdid[igc]) && (triggered_pktid[24] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[25] == L2_cmd_srcid[igc]) && (triggered_trdid[25] == L2_cmd_trdid[igc]) && (triggered_pktid[25] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[26] == L2_cmd_srcid[igc]) && (triggered_trdid[26] == L2_cmd_trdid[igc]) && (triggered_pktid[26] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[27] == L2_cmd_srcid[igc]) && (triggered_trdid[27] == L2_cmd_trdid[igc]) && (triggered_pktid[27] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[28] == L2_cmd_srcid[igc]) && (triggered_trdid[28] == L2_cmd_trdid[igc]) && (triggered_pktid[28] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[29] == L2_cmd_srcid[igc]) && (triggered_trdid[29] == L2_cmd_trdid[igc]) && (triggered_pktid[29] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[30] == L2_cmd_srcid[igc]) && (triggered_trdid[30] == L2_cmd_trdid[igc]) && (triggered_pktid[30] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[31] == L2_cmd_srcid[igc]) && (triggered_trdid[31] == L2_cmd_trdid[igc]) && (triggered_pktid[31] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[32] == L2_cmd_srcid[igc]) && (triggered_trdid[32] == L2_cmd_trdid[igc]) && (triggered_pktid[32] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[33] == L2_cmd_srcid[igc]) && (triggered_trdid[33] == L2_cmd_trdid[igc]) && (triggered_pktid[33] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[34] == L2_cmd_srcid[igc]) && (triggered_trdid[34] == L2_cmd_trdid[igc]) && (triggered_pktid[34] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[35] == L2_cmd_srcid[igc]) && (triggered_trdid[35] == L2_cmd_trdid[igc]) && (triggered_pktid[35] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[36] == L2_cmd_srcid[igc]) && (triggered_trdid[36] == L2_cmd_trdid[igc]) && (triggered_pktid[36] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[37] == L2_cmd_srcid[igc]) && (triggered_trdid[37] == L2_cmd_trdid[igc]) && (triggered_pktid[37] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[38] == L2_cmd_srcid[igc]) && (triggered_trdid[38] == L2_cmd_trdid[igc]) && (triggered_pktid[38] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[39] == L2_cmd_srcid[igc]) && (triggered_trdid[39] == L2_cmd_trdid[igc]) && (triggered_pktid[39] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[40] == L2_cmd_srcid[igc]) && (triggered_trdid[40] == L2_cmd_trdid[igc]) && (triggered_pktid[40] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[41] == L2_cmd_srcid[igc]) && (triggered_trdid[41] == L2_cmd_trdid[igc]) && (triggered_pktid[41] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[42] == L2_cmd_srcid[igc]) && (triggered_trdid[42] == L2_cmd_trdid[igc]) && (triggered_pktid[42] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[43] == L2_cmd_srcid[igc]) && (triggered_trdid[43] == L2_cmd_trdid[igc]) && (triggered_pktid[43] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[44] == L2_cmd_srcid[igc]) && (triggered_trdid[44] == L2_cmd_trdid[igc]) && (triggered_pktid[44] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[45] == L2_cmd_srcid[igc]) && (triggered_trdid[45] == L2_cmd_trdid[igc]) && (triggered_pktid[45] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[46] == L2_cmd_srcid[igc]) && (triggered_trdid[46] == L2_cmd_trdid[igc]) && (triggered_pktid[46] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[47] == L2_cmd_srcid[igc]) && (triggered_trdid[47] == L2_cmd_trdid[igc]) && (triggered_pktid[47] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[48] == L2_cmd_srcid[igc]) && (triggered_trdid[48] == L2_cmd_trdid[igc]) && (triggered_pktid[48] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[49] == L2_cmd_srcid[igc]) && (triggered_trdid[49] == L2_cmd_trdid[igc]) && (triggered_pktid[49] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[50] == L2_cmd_srcid[igc]) && (triggered_trdid[50] == L2_cmd_trdid[igc]) && (triggered_pktid[50] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[51] == L2_cmd_srcid[igc]) && (triggered_trdid[51] == L2_cmd_trdid[igc]) && (triggered_pktid[51] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[52] == L2_cmd_srcid[igc]) && (triggered_trdid[52] == L2_cmd_trdid[igc]) && (triggered_pktid[52] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[53] == L2_cmd_srcid[igc]) && (triggered_trdid[53] == L2_cmd_trdid[igc]) && (triggered_pktid[53] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[54] == L2_cmd_srcid[igc]) && (triggered_trdid[54] == L2_cmd_trdid[igc]) && (triggered_pktid[54] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[55] == L2_cmd_srcid[igc]) && (triggered_trdid[55] == L2_cmd_trdid[igc]) && (triggered_pktid[55] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[56] == L2_cmd_srcid[igc]) && (triggered_trdid[56] == L2_cmd_trdid[igc]) && (triggered_pktid[56] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[57] == L2_cmd_srcid[igc]) && (triggered_trdid[57] == L2_cmd_trdid[igc]) && (triggered_pktid[57] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[58] == L2_cmd_srcid[igc]) && (triggered_trdid[58] == L2_cmd_trdid[igc]) && (triggered_pktid[58] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[59] == L2_cmd_srcid[igc]) && (triggered_trdid[59] == L2_cmd_trdid[igc]) && (triggered_pktid[59] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[60] == L2_cmd_srcid[igc]) && (triggered_trdid[60] == L2_cmd_trdid[igc]) && (triggered_pktid[60] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[61] == L2_cmd_srcid[igc]) && (triggered_trdid[61] == L2_cmd_trdid[igc]) && (triggered_pktid[61] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[62] == L2_cmd_srcid[igc]) && (triggered_trdid[62] == L2_cmd_trdid[igc]) && (triggered_pktid[62] == L2_cmd_pktid[igc])) ||
		    ((triggered_srcid[63] == L2_cmd_srcid[igc]) && (triggered_trdid[63] == L2_cmd_trdid[igc]) && (triggered_pktid[63] == L2_cmd_pktid[igc])))))
		begin
		   dump_L2_vci(L2_vci_cmd[igc], L2_s_phy_addr[igc], L2_cmd_srcid[igc], L2_cmd_trdid[igc], L2_cmd_pktid[igc], clock, igc);
		   prev_L2_phy_addr[igc] <= L2_s_phy_addr[igc];
		   prev_L2_cmd_srcid[igc] <= L2_cmd_srcid[igc];
		   prev_L2_cmd_trdid[igc] <= L2_cmd_trdid[igc];
		   prev_L2_cmd_pktid[igc] <= L2_cmd_pktid[igc];
		end // if (in_main_prog == 1)

		   */
		end // if ((in_main_prog == 1) && (detailled_interest_section >= 20) &&...
	   end // always @ (posedge clk)
end // block: module_gen_cluster
   endgenerate							
   
   always @(posedge clk)
     begin
	if (clock>100000 && reset_n==1 && addr_loaded==0)
	  begin
	     load_trig_addresses(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, barrier_addr, barrier_wait_end_addr, 
				 barrier_instr_futex_wake_addr, main_start_addr, ll_instr_addr, ll_instr_addr_2, futex_wake_addr, 
				 fct_uname_1_addr, fct_uname_2_addr, fct_uname_3_addr,  start_interest_section_addr, end_interest_section_addr, 
				 return_main_addr, trig_instr_1, mask_trig_instr_1, trig_instr_2, mask_trig_instr_2, detailled_interest_section);	     
	     
	     dump_signals(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, main_start_addr, ll_instr_addr, 2000, 2000, 100);

	     addr_loaded = 1;
	     
	     for (i=0; i<64; i++) begin
		r31_bank[i] <= 0;
		ret_futex_wake_bank[i] <= 0;
		ret_uname_bank_1[i] <= 0;
		ret_uname_bank_2[i] <= 0;
		ret_uname_bank_3[i] <= 0;
		prev_pc_bank[i] <= 0;
		prev_L1_phy_addr[i] <= 0;
		triggered_srcid[i] <= 0;
		triggered_trdid[i] <= 0;
		triggered_pktid[i] <= 0;
	     end

	     for (i=0; i<16; i++) begin
		prev_L2_phy_addr[i] <= 0;
		prev_L2_cmd_srcid[i] <= 0;
		prev_L2_cmd_trdid[i] <= 0;
		prev_L2_cmd_pktid[i] <= 0;
		L2_cmd_in_time[i] <= 0;
	     end
	     
	  end; // if (reset_n==0 && addr_loaded==0)
	if (reset_n==0)
	  begin
	     addr_loaded = 0;
	     dump_signals(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, main_start_addr, ll_instr_addr, 2000, 2000, 120);
	  end;
	
	/*
	  if (addr_loaded == 1)
	  begin
	      // debug : verif addr
	     dump_signals(mutex_lock_addr, mutex_unlock_addr, lock_wait_addr, lock_wait_priv_addr, main_start_addr, 2000, 100);
	     addr_loaded = 0;
	  end;
	*/
	clock <= clock + 1;
     end // always @ (posedge clk)

   
   always @(posedge clk_bis)
     begin
	clock_bis <= clock_bis + 1;
	if ((clock_bis < clock-1) || (clock_bis > clock+1))
	  begin
	     dump_signals(clock, 32'hFFFFFFFF, clock_bis, 32'hBEEFFEAD, main_start_addr, ll_instr_addr, clock, clock_bis, 130);
	     clock_bis <= clock;
	  end
     end
endmodule // dump_signals_wrapper
