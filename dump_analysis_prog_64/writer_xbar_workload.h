#ifndef __DEF_WRITER_XBAR_WORKLOAD_H__
#define __DEF_WRITER_XBAR_WORKLOAD_H__

#define DUMP_FILE_XBAR_WORKLOAD_NAME "dump_xbar_workload_prog"

void *thread_writer_xbar_workload(void *arg);

#endif /*__DEF_WRITER_XBAR_WORKLOAD_H__*/
