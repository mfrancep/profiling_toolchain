#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>

#include "writer.h"
#include "structs.h"

#ifdef DEBUG
#define debug_print(args ...) printf(args)
#else
#define debug_print(args ...)
#endif

/* shared variables */
extern volatile core_signals_t * data_signals;
extern std::vector <struct addresses> addresses;
extern uint8_t core_number;

/* global variables */
extern volatile bool exit_flag; //ending request

/* dump files */
std::ofstream dump_file;

bool alert_clock = false;

/* flag to indicate a switch of executable */
bool switch_exe = false;
/* num of the current exe */
uint8_t num_exe = 0;

/* create text files and write the headers of the dump signal in a file procX_signals.txt*/
void dumpfile_headers()
{
	char file_path[256];
	snprintf(file_path, 256, "./dump_files/%s%d.txt", DUMP_FILE_NAME, num_exe);
	dump_file.open(file_path);
	dump_file << "#clock;proc;pc;r6;r28;r29;r31;instruction_code;clock_bis\n";
}

/* write dump signal in a file procX_signals.txt */
void write_signals(core_signals_t &proc_signals, uint8_t proc)
{
  if (!dump_file.is_open())
    dumpfile_headers();

  dump_file << proc_signals.clock << ";"
	    << (uint32_t)proc << ";" 
	    << proc_signals.pc << ";" 
	    << proc_signals.r6 << ";"
	    << proc_signals.r28 << ";"
	    << proc_signals.r29 << ";"
	    << proc_signals.r31 << ";"
	    << proc_signals.instr << ";"
	    << proc_signals.clock_bis << "\n";
}

void * thread_writer(void *arg)
{
	uint8_t proc;
	uint64_t * old_clocks = (uint64_t *)malloc(core_number*sizeof(uint64_t));
	memset(old_clocks, 0, core_number*sizeof(uint32_t));
	// local copies of the shared mem
	core_signals_t * l_data_signals = new core_signals_t [core_number];

	printf("thread writer, core number %d\n", core_number);
	
	// create and open dump files
	dumpfile_headers();

	while(!exit_flag)
	{
		/* we work on local copy of share mem */
		//memcpy((core_signals_t*)l_data_signals, (core_signals_t*)data_signals, core_number*sizeof(core_signals_t));

		if (switch_exe)
		{
			// resert flag
			switch_exe = false;
			// close old dump_file
			dump_file.close();
			// create and open dump files
			dumpfile_headers();
		}

		if (alert_clock)
		{
			dump_file << "#alert clock\n";
			alert_clock = false;
		}
		
		for(proc=0; proc<core_number; proc++)
		{
		  if(data_signals[proc].clock == old_clocks[proc])
			continue;
		  /* we work on local copy of share mem */
		  memcpy((core_signals_t*)&l_data_signals[proc], (core_signals_t*)&data_signals[proc], sizeof(core_signals_t));

		  // update old clock value
		  old_clocks[proc] = l_data_signals[proc].clock;

		  // pc can't be null during executing flow
		  if(l_data_signals[proc].pc == 0)
		    continue;

		  // write signals in files
		  write_signals(l_data_signals[proc], proc);

		  debug_print("! analyse loop clock %ld (proc %u)!\n", l_data_signals[proc].clock, proc);
		}
		
	}

	// close dump files
	dump_file.close();
	delete [] l_data_signals;
	free(old_clocks);
	
	return NULL;
}
