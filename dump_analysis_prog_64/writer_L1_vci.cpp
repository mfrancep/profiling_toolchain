#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>

#include "writer_L1_vci.h"
#include "structs.h"

#ifdef DEBUG
#define debug_print(args ...) printf(args)
#else
#define debug_print(args ...)
#endif

/* shared variables */
extern volatile vci_t * L1_vci;
extern uint8_t core_number;
/* num of the current exe */
extern uint8_t num_exe;

/* global variables */
extern volatile bool exit_flag; //ending request

/* dump files */
std::ofstream dump_file_L1_vci;

/* flag to indicate a switch of executable */
bool switch_exe_L1_vci = false;

/* create text files and write the headers of the dump signal in a file procX_signals.txt*/
static void dumpfile_L1_vci_headers()
{
	char file_path[256];
	snprintf(file_path, 256, "./dump_files/%s%d.txt", DUMP_FILE_L1_VCI_NAME, num_exe);
	dump_file_L1_vci.open(file_path);
	dump_file_L1_vci << "#clock;proc;pc;inst;phy_addr;cmd;srcid;trdid;pktid\n";
}

/* write dump signal in a file procX_signals.txt */
static void write_L1_vci(vci_t &vci, uint8_t proc)
{
  if (!dump_file_L1_vci.is_open())
    dumpfile_L1_vci_headers();
  
  dump_file_L1_vci << vci.clock << ";"
		   << (uint32_t)proc << ";"
		   << vci.pc << ";"
		   << vci.inst << ";"
		   << vci.phy_addr << ";"
		   << vci.vci_cmd << ";"
		   << vci.srcid << ";"
		   << vci.trdid << ";"
		   << vci.pktid << "\n";
}

void * thread_writer_L1_vci(void *arg)
{
	uint8_t core;
	uint32_t * old_clocks = (uint32_t *)malloc((core_number)*sizeof(uint32_t));
	memset(old_clocks, 0, (core_number)*sizeof(uint32_t));
	// local copies of the shared mem
	vci_t * l_data_vci = new vci_t [core_number];

	printf("thread writer L1 vci, core number %d cluster %d\n", core_number, core_number/4);
	
	// create and open dump files
	dumpfile_L1_vci_headers();

	while(!exit_flag)
	{
		if (switch_exe_L1_vci)
		{
			// resert flag
			switch_exe_L1_vci = false;
			// close old dump_file
			dump_file_L1_vci.close();
			// create and open dump files
			dumpfile_L1_vci_headers();
		}
		
		for(core=0; core<core_number; core++)
		{
		  if(L1_vci[core].clock == old_clocks[core])
			continue;
		  /* we work on local copy of share mem */
		  memcpy((vci_t*)&l_data_vci[core], (vci_t*)&L1_vci[core], sizeof(vci_t));

		  // update old clock value
		  old_clocks[core] = l_data_vci[core].clock;

		  // write signals in files
		  write_L1_vci(l_data_vci[core], core);

		  debug_print("! analyse vci loop clock %ld (core %u)!\n", l_data_vci[core].clock, core);
		}
		
	}

	// close dump files
	dump_file_L1_vci.close();
	delete [] l_data_vci;
	free(old_clocks);
	
	return NULL;
}
