#include <iostream>
#include <stdio.h>
#include <map>
#include <csignal>
#include <fstream>
#include <string.h>
#include <sstream>
#include <stdlib.h>

#include "structs.h"
//#include "data_analysis.h"
#include "writer.h"
#include "writer_L1_vci.h"
#include "writer_L2_vci.h"
#include "writer_L2_workload.h"
#include "writer_xbar_workload.h"

/* global variables */
volatile bool exit_flag = false; //ending request

/* flag to indicate if addresses file have been loaded */
bool addresses_loaded = false;

/* shared variables */
volatile core_signals_t * data_signals;
volatile vci_t * L1_vci;
volatile vci_t * L2_vci;
volatile L2_workload_t * L2_workload;
volatile xbar_workload_t * xbar_workload;
std::vector <struct addresses> addresses;
uint8_t core_number = 0;
pthread_mutex_t *mutexes;

unsigned int interest_section_fine_step = 0;

uint32_t * old_pc;

//dumpfile file descriptor
extern std::ofstream dump_file;
extern uint8_t num_exe;
uint8_t num_int = 0;

void signal_interrupt_handler( int signum )
{
	std::cout << "!!!! Interrupt signal (" << signum << ") received. !!!!\n";
	// if SIGINT receive we create a new dumpfile
	if (signum == SIGINT)
	{
		// close old dump files
		dump_file.close();
		char file_path[256];
		snprintf(file_path, 256, "int_%d_%s%d.txt", num_int, DUMP_FILE_NAME, num_exe);
		dump_file.open(file_path);
		dump_file << "#clock;proc;pc;r6;r28;r29;r31,instruction_code\n";
		num_int++;
	}
	else
	{
		// terminate program
		exit_flag = true;
		printf("exit flag set to true\n");
	}

	signal(SIGINT, signal_interrupt_handler);
	signal(SIGTERM, signal_interrupt_handler);
	signal(SIGKILL, signal_interrupt_handler);
	signal(SIGQUIT, signal_interrupt_handler);
}

uint32_t string2int(std::string str)
{
	unsigned int x;   
	std::stringstream ss;
	ss << std::dec << str;
	ss >> x;
	return x;
}

int read_addr_file(char * file_path)
{
	std::ifstream addrFile;
	addrFile.open (file_path);
	if (!addrFile.is_open())
	  {
	    fprintf(stderr, "can't open addresses files\n");
	    return -1;
	  }
	std::string line;
	std::string subline;
	struct addresses addr;
	while(std::getline(addrFile, line))
	{
		if (line.find("#") != std::string::npos){
			subline = line.substr(line.find("#")+1);
			std::cout << "exectutable name " << subline << "\n"; 
		}
		else if (line.find("mutex_lock_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mutex_lock_addr = string2int(subline);
		}
		else if (line.find("mutex_unlock_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mutex_unlock_addr = string2int(subline);
		}
		else if (line.find("lock_wait_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.lock_wait_addr = string2int(subline);
		}
		else if (line.find("lock_wait_priv_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.lock_wait_priv_addr = string2int(subline);
		}
		else if (line.find("barrier_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.barrier_addr = string2int(subline);
		}
		else if (line.find("barrier_wait_end_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.barrier_wait_end_addr = string2int(subline);
		}
		else if (line.find("barrier_instr_futex_wake_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.barrier_instr_futex_wake_addr = string2int(subline);
		}
		else if (line.find("main_start_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.main_start_addr = string2int(subline);
		}
		else if (line.find("main_return_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.main_return_addr = string2int(subline);
		}
		else if (line.find("ll_instr_addr:") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.ll_instr_addr = string2int(subline);
		}
		else if (line.find("ll_instr_addr_2:") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.ll_instr_addr_2 = string2int(subline);
		}
		else if (line.find("futex_wake") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.futex_wake_addr = string2int(subline);
		}
		else if (line.find("fct_uname_1") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.fct_uname_1_addr = string2int(subline);
		}
		else if (line.find("fct_uname_2") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.fct_uname_2_addr = string2int(subline);
		}
		else if (line.find("fct_uname_3") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.fct_uname_3_addr = string2int(subline);
		}
		else if (line.find("start_interest_section") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.start_interest_section_addr = string2int(subline);
		}
		else if (line.find("end_interest_section") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.end_interest_section_addr = string2int(subline);
		}
		else if (line.find("trig_instr_1") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.trig_instr_1 = string2int(subline);
		}
		else if (line.find("mask_instr_1") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mask_trig_instr_1 = string2int(subline);
		}
		else if (line.find("trig_instr_2") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.trig_instr_2 = string2int(subline);
		}
		else if (line.find("mask_instr_2") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mask_trig_instr_2 = string2int(subline);
			//last address, we push all in the vector
			addresses.push_back(addr);
		}
	}
	addresses_loaded = true;
	addrFile.close();
	return 1;
}

void * dump_core(void *arg)
{
      // read configuration file
	std::ifstream myfile;
	myfile.open("inputs.conf");
	if (!myfile.is_open())
	{
		printf("input.conf not open\n");
		return NULL;
	}
	printf("read inputs from config files\n");
	std::string line;
      while(std::getline(myfile, line))
      {
	      if (line.find("core number") != std::string::npos){
		      std::string data = line.substr(line.find("=")+1);
		      core_number = string2int(data);
		      printf("core_number %d\n", core_number); 
	      }
	      if (line.find("fine gain interest section") != std::string::npos){
		      std::string data = line.substr(line.find("=")+1);
		      interest_section_fine_step = string2int(data);
		      printf("interest_section_fine_step %d\n", interest_section_fine_step); 
	      }
      }
      myfile.close();
      
      printf("Start analysing signals for %d core\n", core_number);
      
      if (read_addr_file((char*)ADDR_FILE_PATH) < 0)
	  return NULL;

      /* shared memory struct initialization */
      data_signals = new core_signals_t [core_number];
      memset((void*)data_signals, 0, core_number*sizeof(core_signals_t));
      old_pc = (uint32_t*)malloc(core_number*sizeof(uint32_t));
      memset((void*)old_pc, 0, core_number*sizeof(uint32_t));

      L1_vci = new vci_t[core_number];
      memset((void*)L1_vci, 0, (core_number)*sizeof(vci_t));

      L2_vci = new vci_t[core_number/4];
      memset((void*)L2_vci, 0, (core_number/4)*sizeof(vci_t));

      L2_workload = new L2_workload_t[core_number/4];
      memset((void*)L2_workload, 0, (core_number/4)*sizeof(L2_workload_t));

      xbar_workload = new xbar_workload_t[core_number/4];
      memset((void*)xbar_workload, 0, (core_number/4)*sizeof(xbar_workload_t));

      mutexes = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t)*(core_number/4));
      for (int i =0; i < (core_number/4); i++)
	      pthread_mutex_init(&mutexes[i], NULL);
      
      exit_flag = false;
      
      signal(SIGINT, signal_interrupt_handler);
      signal(SIGTERM, signal_interrupt_handler);
      signal(SIGKILL, signal_interrupt_handler);
      signal(SIGQUIT, signal_interrupt_handler);
	  
      // run threads : analyse thread and signals dumping thread 
      pthread_t write_thread;
      pthread_t write_L1_vci_thread;
      pthread_t write_L2_vci_thread;
      pthread_t write_L2_workload_thread;
      pthread_t write_xbar_workload_thread;
      // run analyse thread
      if(pthread_create(&write_thread , NULL , thread_writer , NULL) < 0)
      {
	      perror("could not create thread analyse");
	      delete [] data_signals;
	      delete [] L1_vci;
	      delete [] L2_vci;
	      delete [] L2_workload;
	      delete [] xbar_workload;
	      free(old_pc);
	      return NULL;
      }

      // run addr thread
      if(pthread_create(&write_L1_vci_thread, NULL , thread_writer_L1_vci , NULL) < 0)
      {
	      perror("could not create thread addr");
	      delete [] data_signals;
	      delete [] L1_vci;
	      delete [] L2_vci;
	      delete [] L2_workload;
	      delete [] xbar_workload;
	      free(old_pc);
	      return NULL;
      }

      // run addr thread
      if(pthread_create(&write_L2_vci_thread, NULL , thread_writer_L2_vci , NULL) < 0)
      {
	      perror("could not create thread addr");
	      delete [] data_signals;
	      delete [] L1_vci;
	      delete [] L2_vci;
	      delete [] L2_workload;
	      delete [] xbar_workload;
	      free(old_pc);
	      return NULL;
      }

      // run L2 workload thread
      if(pthread_create(&write_L2_workload_thread, NULL , thread_writer_L2_workload , NULL) < 0)
      {
	      perror("could not create thread L2 workload");
	      delete [] data_signals;
	      delete [] L1_vci;
	      delete [] L2_vci;
	      delete [] L2_workload;
	      delete [] xbar_workload;
	      free(old_pc);
	      return NULL;
      }

       // run xbar workload thread
      if(pthread_create(&write_xbar_workload_thread, NULL , thread_writer_xbar_workload , NULL) < 0)
      {
	      perror("could not create thread xbar workload");
	      delete [] data_signals;
	      delete [] L1_vci;
	      delete [] L2_vci;
	      delete [] L2_workload;
	      delete [] xbar_workload;
	      free(old_pc);
	      return NULL;
      }
	      
      //Now join the threads, so that we dont terminate before the thread
      pthread_join(write_thread , NULL);
      pthread_join(write_L1_vci_thread , NULL);
      pthread_join(write_L2_vci_thread , NULL);
      pthread_join(write_L2_workload_thread , NULL);
      pthread_join(write_xbar_workload_thread , NULL);
      
      delete [] data_signals;
      delete [] L1_vci;
      delete [] L2_vci;
      delete [] L2_workload;
      delete [] xbar_workload;
      free(old_pc);

      return NULL;
}
