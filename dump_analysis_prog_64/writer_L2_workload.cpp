#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include "writer_L2_workload.h"
#include "structs.h"

#ifdef DEBUG
#define debug_print(args ...) printf(args)
#else
#define debug_print(args ...)
#endif

/* shared variables */
extern volatile L2_workload_t * L2_workload;
extern uint8_t core_number;
/* num of the current exe */
extern uint8_t num_exe;

/* global variables */
extern volatile bool exit_flag; //ending request

/* dump files */
std::ofstream dump_file_L2_workload;

/* flag to indicate a switch of executable */
bool switch_exe_L2_workload = false;

/* create text files and write the headers of the dump signal in a file procX_signals.txt*/
static void dumpfile_L2_workload_headers()
{
	char file_path[256];
	snprintf(file_path, 256, "./dump_files/%s%d.txt", DUMP_FILE_L2_WORKLOAD_NAME, num_exe);
	dump_file_L2_workload.open(file_path);
	dump_file_L2_workload << "#clock;cluster;in_time;ack_time\n";
}

/* write dump signal in a file procX_signals.txt */
static void write_L2_workload(L2_workload_t &workload, uint8_t cluster)
{
	if (!dump_file_L2_workload.is_open())
		dumpfile_L2_workload_headers();

	dump_file_L2_workload << workload.clock << ";"
			      << (uint32_t)cluster << ";"
			      << workload.L2_cmd_in_time << ";"
			      << workload.clock << "\n";
}

void * thread_writer_L2_workload(void *arg)
{
	uint8_t cluster;
	uint32_t * old_clocks = (uint32_t *)malloc((core_number/4)*sizeof(uint32_t));
	memset(old_clocks, 0, (core_number/4)*sizeof(uint32_t));
	// local copies of the shared mem
	L2_workload_t * l_data_workload = new L2_workload_t [core_number/4];

	printf("thread writer L2 workload, core number %d cluster %d\n", core_number, core_number/4);
	
	// create and open dump files
	dumpfile_L2_workload_headers();

	while(!exit_flag)
	{
		if (switch_exe_L2_workload)
		{
			// resert flag
			switch_exe_L2_workload = false;
			// close old dump_file
			dump_file_L2_workload.close();
			// create and open dump files
			dumpfile_L2_workload_headers();
		}
		
		for(cluster=0; cluster<core_number/4; cluster++)
		{
		  if(L2_workload[cluster].clock == old_clocks[cluster])
			continue;
		  /* we work on local copy of share mem */
		  memcpy((L2_workload_t*)&l_data_workload[cluster], (vci_t*)&L2_workload[cluster], sizeof(L2_workload_t));

		  // update old clock value
		  old_clocks[cluster] = l_data_workload[cluster].clock;

		  // write signals in files
		  write_L2_workload(l_data_workload[cluster], cluster);

		  debug_print("! analyse workload loop clock %ld (cluster %u)!\n", l_data_workload[cluster].clock, cluster);
		}
		
	}

	// close dump files
	dump_file_L2_workload.close();
	delete [] l_data_workload;
	free(old_clocks);
	
	return NULL;
}
