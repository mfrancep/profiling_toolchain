#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include "writer_xbar_workload.h"
#include "structs.h"

#ifdef DEBUG
#define debug_print(args ...) printf(args)
#else
#define debug_print(args ...)
#endif

/* shared variables */
extern volatile xbar_workload_t * xbar_workload;
extern uint8_t core_number;
/* num of the current exe */
extern uint8_t num_exe;

/* global variables */
extern volatile bool exit_flag; //ending request

/* dump files */
std::ofstream dump_file_xbar_workload;

/* flag to indicate a switch of executable */
bool switch_exe_xbar_workload = false;

/* create text files and write the headers of the dump signal in a file procX_signals.txt*/
static void dumpfile_xbar_workload_headers()
{
	char file_path[256];
	snprintf(file_path, 256, "./dump_files/%s%d.txt", DUMP_FILE_XBAR_WORKLOAD_NAME, num_exe);
	dump_file_xbar_workload.open(file_path);
	dump_file_xbar_workload << "#clock;cluster;pending_requestsx\n";
}

/* write dump signal in a file procX_signals.txt */
static void write_xbar_workload(xbar_workload_t &workload, uint8_t cluster)
{
	if (!dump_file_xbar_workload.is_open())
		dumpfile_xbar_workload_headers();

	dump_file_xbar_workload << workload.clock << ";"
			      << (uint32_t)cluster << ";"
			      << workload.xbar_req << "\n";
}

void * thread_writer_xbar_workload(void *arg)
{
	uint8_t cluster;
	uint32_t * old_clocks = (uint32_t *)malloc((core_number/4)*sizeof(uint32_t));
	memset(old_clocks, 0, (core_number/4)*sizeof(uint32_t));
	// local copies of the shared mem
	xbar_workload_t * l_data_workload = new xbar_workload_t [core_number/4];

	printf("thread writer xbar workload, core number %d cluster %d\n", core_number, core_number/4);
	
	// create and open dump files
	dumpfile_xbar_workload_headers();

	while(!exit_flag)
	{
		if (switch_exe_xbar_workload)
		{
			// resert flag
			switch_exe_xbar_workload = false;
			// close old dump_file
			dump_file_xbar_workload.close();
			// create and open dump files
			dumpfile_xbar_workload_headers();
		}
		
		for(cluster=0; cluster<core_number/4; cluster++)
		{
		  if(xbar_workload[cluster].clock == old_clocks[cluster])
			continue;
		  /* we work on local copy of share mem */
		  memcpy((xbar_workload_t*)&l_data_workload[cluster], (vci_t*)&xbar_workload[cluster], sizeof(xbar_workload_t));

		  // update old clock value
		  old_clocks[cluster] = l_data_workload[cluster].clock;

		  // write signals in files
		  write_xbar_workload(l_data_workload[cluster], cluster);

		  debug_print("! analyse workload loop clock %ld (cluster %u)!\n", l_data_workload[cluster].clock, cluster);
		}
		
	}

	// close dump files
	dump_file_xbar_workload.close();
	delete [] l_data_workload;
	free(old_clocks);
	
	return NULL;
}
