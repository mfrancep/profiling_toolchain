#include <iostream>
#include <stdio.h>

#include "structs.h"
#include "svdpi.h"
#include "tbxbindings.h"

/* shared variables */
extern volatile vci_t * L1_vci;
extern uint8_t core_number;

int dump_L1_vci(const svBitVecVal* pc, const svBitVecVal* inst, const svBitVecVal* vci_cmd, const svBitVecVal* phy_addr, const svBitVecVal* srcid, const svBitVecVal* trdid, const svBitVecVal* pktid, unsigned int clock, unsigned int proc_id)
{
	if (proc_id < core_number)
	{
		L1_vci[proc_id].pc = (uint32_t)*pc;
		L1_vci[proc_id].inst = (uint32_t)*inst;
		L1_vci[proc_id].clock = clock;
		L1_vci[proc_id].vci_cmd = (uint32_t) (*vci_cmd);
		L1_vci[proc_id].srcid = (uint32_t) (*srcid);
		L1_vci[proc_id].trdid = (uint32_t) (*trdid);
		L1_vci[proc_id].pktid = (uint32_t) (*pktid);
		L1_vci[proc_id].phy_addr = (uint64_t)*phy_addr;
		printf("(ADDR] proc %d => clock %d : pc %X / vci_cmd %d / phy_addr %lX \n", proc_id, clock, L1_vci[proc_id].pc, L1_vci[proc_id].vci_cmd, L1_vci[proc_id].phy_addr);
	}
	return 1;
}
