#include <iostream>
#include <stdio.h>

#include "structs.h"
#include "svdpi.h"
#include "tbxbindings.h"

/* shared variables */
extern volatile L2_workload_t * L2_workload;
extern uint8_t core_number;

int dump_L2_workload(const svBitVecVal* L2_cmd_in_time, unsigned int clock, unsigned int cluster_id)
{
	if (cluster_id < (core_number/4))
	{
		L2_workload[cluster_id].L2_cmd_in_time = (uint32_t) * L2_cmd_in_time;
		L2_workload[cluster_id].clock = clock;
		L2_workload[cluster_id].cluster_id = cluster_id;
		printf("[WORKLOAD] proc %d => clock %d : in_time %d \n", cluster_id, clock, L2_workload[cluster_id].L2_cmd_in_time);
	}
	return 1;
}
