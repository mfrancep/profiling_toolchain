#ifndef _DEF_STRUCTS_H__
#define  _DEF_STRUCTS_H__

#include <stdint.h>
#include <vector>

#define ADDR_FILE_PATH "dump_analysis_prog/extracted_addresses.txt"

struct addresses{
	uint32_t mutex_lock_addr;
	uint32_t mutex_unlock_addr;
	uint32_t lock_wait_addr;
	uint32_t lock_wait_priv_addr;
	uint32_t barrier_addr;
	uint32_t barrier_wait_end_addr;
	uint32_t barrier_instr_futex_wake_addr;
	uint32_t main_start_addr;
	uint32_t main_return_addr;
	uint32_t ll_instr_addr;
	uint32_t ll_instr_addr_2;
	uint32_t futex_wake_addr;
	uint32_t irq_routine_addr;
	uint32_t irq_routine_ret_addr;
	uint32_t fct_uname_1_addr;
	uint32_t fct_uname_2_addr;
	uint32_t fct_uname_3_addr;
	uint32_t start_interest_section_addr;
	uint32_t end_interest_section_addr;
	uint32_t trig_instr_1;
	uint32_t mask_trig_instr_1;
	uint32_t trig_instr_2;
	uint32_t mask_trig_instr_2;
};

typedef struct {
	uint32_t pc;
	uint32_t r6; //a2
	uint32_t r28; //gp
	uint32_t r29; //sp
	uint32_t r31; //ra
	uint64_t clock; // clock tick associated to register value
	uint64_t clock_bis; // clock tick associated to register value
	uint32_t instr; // instruction code
} core_signals_t;

struct lock_dyn {
	uint64_t time_req;
	uint8_t proc;
};

typedef struct {
	uint32_t contention;
	std::vector <struct lock_dyn> dyn;
} locks_info_t;

struct lock_delays{
	std::vector <uint64_t> acquire_delays;
	std::vector <uint64_t> cs_times;
	std::vector <uint64_t> go_to_sleep_delays;
	std::vector <uint64_t> contented_release_delays;
	std::vector <uint64_t> interlock_delays;
};

struct barrier_delays{
	std::vector <uint64_t> arrival_times;
	std::vector <uint64_t> wakeup_times;
	std::vector <uint64_t> aware_delays;
	std::vector <uint64_t> aware_2_first_release_delays;
	std::vector <uint64_t> aware_2_last_release_delays;
	std::vector <uint64_t> arrival_2_last_release_delays;
};

typedef struct {
	struct lock_delays lock;
	struct barrier_delays barrier;
} delays_t;

struct thread_id {
	uint32_t gp; /* PID image */
	uint32_t fp; /* TID image */
};

typedef struct {
	struct thread_id tid;
	uint32_t data;
} signed_data_t;

typedef struct {
	uint32_t pc;
	uint32_t inst;
	uint32_t vci_cmd;
	uint64_t phy_addr;
	uint64_t clock;
	uint32_t cluster_id;
	uint32_t trdid;
	uint32_t pktid;
	uint32_t srcid;
} vci_t;

typedef struct {
	uint32_t L2_cmd_in_time;
	uint64_t clock;
	uint32_t cluster_id;
} L2_workload_t;

typedef struct {
	uint32_t xbar_req;
	uint64_t clock;
	uint32_t cluster_id;
} xbar_workload_t;


#endif /* _DEF_STRUCTS_H__ */
