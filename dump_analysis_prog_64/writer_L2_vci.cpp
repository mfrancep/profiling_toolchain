#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include "writer_L2_vci.h"
#include "structs.h"

#ifdef DEBUG
#define debug_print(args ...) printf(args)
#else
#define debug_print(args ...)
#endif

/* shared variables */
extern volatile vci_t * L2_vci;
extern uint8_t core_number;
/* num of the current exe */
extern uint8_t num_exe;
extern pthread_mutex_t *mutexes;

/* global variables */
extern volatile bool exit_flag; //ending request

/* dump files */
std::ofstream dump_file_L2_vci;

/* flag to indicate a switch of executable */
bool switch_exe_L2_vci = false;

/* create text files and write the headers of the dump signal in a file procX_signals.txt*/
static void dumpfile_L2_vci_headers()
{
	char file_path[256];
	snprintf(file_path, 256, "./dump_files/%s%d.txt", DUMP_FILE_L2_VCI_NAME, num_exe);
	dump_file_L2_vci.open(file_path);
	dump_file_L2_vci << "#clock;cluster;phy_addr;cmd;srcid;trdid;pktid\n";
}

/* write dump signal in a file procX_signals.txt */
static void write_L2_vci(vci_t &vci, uint8_t cluster)
{
  if (!dump_file_L2_vci.is_open())
    dumpfile_L2_vci_headers();
  
  dump_file_L2_vci << vci.clock << ";"
		   << (uint32_t)cluster << ";"
		   << vci.phy_addr << ";"
		   << vci.vci_cmd << ";"
		   << vci.srcid << ";"
		   << vci.trdid << ";"
		   << vci.pktid << "\n";
}

void * thread_writer_L2_vci(void *arg)
{
	uint8_t cluster;
	uint32_t * old_clocks = (uint32_t *)malloc((core_number/4)*sizeof(uint32_t));
	memset(old_clocks, 0, (core_number/4)*sizeof(uint32_t));
	// local copies of the shared mem
	vci_t * l_data_vci = new vci_t [core_number/4];

	printf("thread writer L2 vci, core number %d cluster %d\n", core_number, core_number/4);
	
	// create and open dump files
	dumpfile_L2_vci_headers();

	while(!exit_flag)
	{
		if (switch_exe_L2_vci)
		{
			// resert flag
			switch_exe_L2_vci = false;
			// close old dump_file
			dump_file_L2_vci.close();
			// create and open dump files
			dumpfile_L2_vci_headers();
		}
		
		for(cluster=0; cluster<core_number/4; cluster++)
		{
		  if(L2_vci[cluster].clock == old_clocks[cluster])
			continue;
		  /* we work on local copy of share mem */
		  pthread_mutex_lock(&mutexes[cluster]);
		  memcpy((vci_t*)&l_data_vci[cluster], (vci_t*)&L2_vci[cluster], sizeof(vci_t));
		  pthread_mutex_unlock(&mutexes[cluster]);
		  
		  // update old clock value
		  old_clocks[cluster] = l_data_vci[cluster].clock;

		  // write signals in files
		  write_L2_vci(l_data_vci[cluster], cluster);

		  debug_print("! analyse vci loop clock %ld (cluster %u)!\n", l_data_vci[cluster].clock, cluster);
		}
		
	}

	// close dump files
	dump_file_L2_vci.close();
	delete [] l_data_vci;
	free(old_clocks);
	
	return NULL;
}
